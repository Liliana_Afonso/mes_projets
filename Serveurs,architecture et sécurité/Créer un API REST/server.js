// Importation des modules externes
const express = require("express"); // Module JS permettant de créer des endpoints HTTP facilement
const bodyParser = require("body-parser"); // Module JS permettant de tranformer les paramètres en JSON
const auth = require("./auth");
/*
  Paramètrage d'Express. Pas besoin de toucher.
  ------------------------------------------------
*/
// Paramètrage de Express
const app = express();
app.use(auth);
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type, Authorization, Content-Length, X-Requested-With"
  );
  next();
});
/*
  ------------------------------------------------
*/

/*
  Déclaration des données
*/
//const data = {
const data = {
  items: [
    {
      title: "Item de l'index 0",
      content: "Je suis un contenu",
    },{
    
      title: "Item de l'index 1",
      content: "Je suis un contenu",
    },{
    
      title: "Item de l'index 2",
      content: "Je suis un contenu",
    },{
    
      title: "Item de l'index 3", // J'ai rajouté deux autres éléments dans le tableau car dans l'énoncé, il a été demandé de faire une requête GET pour l'index=5
      content: "Je suis un contenu",
    },{
    
      title: "Item de l'index 4",
      content: "Je suis un contenu",
    },{
    
      title: "Item de l'index 5",
      content: "Je suis un contenu",
    },
  ],
};
/*
  Déclaration des endpoints (également appelés *routes*)

  req: Contient toutes les données de la requête reçue: headers, paramètres GET, paramètres POST etc..
  res: Contient des fonctions permettant de répondre à la requête

  Obtenir les paramètres GET: req.query
  Obtenir les paramètres POST: req.body
  Obtenir les "paramètres" dans l'URL: req.params
  Répondre un message text: res.send("Okay, bien reçu")
  Répondre avec un object jSON: res.json({message: "Okay, bien reçu"})
*/

// Lorsqu'on reçoit une requête GET
// Exemple: curl localhost:8080/?index=5
// TODO: Retourner l'item correspondant à l'index envoyé dans la requête
app.get("/", (req, res) => { // la requête GET a pour objectif de extraire les détails d'une ressource
  const paramsGet = req.query; // création d'une constante invariable "paramsGet" qui a pour valeur "req.query" --> cela correspond au paramètre demandé. Ici cela va prendre la valeur : {index: "5"} (pour l'exemple mentionné)
  console.log('Requete GET :' , {paramsGet} ); // affiche les données entrées dans la partie back
  const text = data.items[paramsGet.index]; // création d'une constante invariable "text" qui pour valeur les données du tableau créer selon l'index entré au clavier
  if (paramsGet.index){ // si un index a été rentré alors 
    res.send(text) // réponse dans la partie client qui envoie le title et le contenu (cf tableau) selon l'index demandé
  } else { // si aucune information sur l'index alors
  res.send(data.items) // réponse dans la partie client qui envoie la liste des items (cf tableau)
//    res.send("Veuillez choisir un index\n") // AUTRE OPTION : réponse dans la partie client qui envoie ce message texte
  }
});

// Lorsqu'on reçoit une requête POST
// Exemple: curl -X POST -H "Content-Type: application/json" localhost:8080 -d '{"title":"Mon titre"}'
// TODO: l'item reçu dans le tableau des items
app.post("/", (req, res) => { // la requête POST a pour objectif de créer une ressource
  const paramsPost = req.body; // création d'une constante invariable "paramsPOST" qui a pour valeur "req.body" --> cela correspond au paramètre que le client a entré. Ici cela va prendre la valeur : {title: "Mon titre"} (pour l'exemple mentionné)
  data.items.push(paramsPost);// On va pusher dans le "tableau" data.items les données entrées
  console.log('Requete POST :' , {paramsPost}); // affiche les données entrées dans la partie back
  res.json(paramsPost); // réponse dans la partie client qui envoie ce qui a été entré
 // res.json(data.items); // AUTRE OPTION : affiche le tableau complet
});


// Lorsqu'on reçoit une requête DELETE
// Exemple: curl -X DELETE localhost:8080/6
// TODO: Supprimer l'item correspondant à l'index envoyé en paramètre d'URL --> selon la consigne du dernier cours, la requête DELETE n'a pas été demandé de faire
app.delete("/:number", (req, res) => { // la requête DELETE a pour objectif de supprimer une ressource
  const paramsURL = req.params; // création d'une constante invariable "paramsURL" qui a pour valeur "req.params" --> cela correspond aux valeurs de paramètres analysées à partir d'un chemin URL. Ici cela va prendre la valeur : {number: "6"} (pour l'exemple mentionné) 
  data.items = data.items.filter((items,index) => index !==parseInt(paramsURL.number)); // filter va créer un tableau rempli de tous les éléments du tableau qui passent un test // parseInt ca va convertir en integer
  console.log({ paramsURL }); // affiche les données entrées dans la partie back
  res.json(paramsURL);// réponse dans la partie client qui envoie ce qui a été entré
});

// Lorsqu'on reçoit une requête PUT
// Exemple: curl -X PUT -H "Content-Type: application/json" localhost:8080/?index=2 -d '{"newTitle":"Mon nouveau titre"}'
// TODO: Modifier l'item correspondant à l'index reçu en paramètre GET avec les données reçues en paramètre POST
app.put("/", (req, res) => { // la requête PUT a pour objectif de mettre à jour ou de créer une ressource
  const paramsGet = req.query; // reprise de la constante invariable "paramsGet" qui a pour valeur "req.query" --> cela correspond au paramètre demandé. Ici cela va prendre la valeur : {index: 2}(pour l'exemple mentionné)
  const paramsPost = req.body; // reprise de la constante invariable "paramsPOST" qui a pour valeur "req.body" --> cela correspond au paramètre que le client a entré. Ici cela va prendre la valeur : {newTitle: "Mon nouveau titre"} (pour l'exemple mentionné)
  data.items[paramsGet.index].title=paramsPost.newTitle; // ici on prend la ligne correspondant à l'index donné avec l'attribut "title" et on remplace le titre par la valeur qu'on a écrit sous le nom "newTitle"
  console.log('Requete PUT :' , {paramsPost} ); // affiche les données entrées dans la partie back
  res.json(paramsPost); // réponse dans la partie client qui envoie ce qui a été entré
});

/*
  Lancement du serveur sur le port 8080
*/
app.listen(8080, () => console.log(`Listen on port 8080`));
