//
//  fonctionnalites_tab.h
//  Projet_Serie_1_2_3_4_Liliana
//
//  Created by Liliana Afonso on 16.11.18.
//  Copyright © 2018 Liliana Afonso. All rights reserved.
//

#ifndef fonctionnalites_tab_h
#define fonctionnalites_tab_h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Déclaration des maximums */
#define MAX_SIZE_TAB 50 /* Défini la taille maximum des tableaux */
#define MAX_SIZE_C 10 /* Défini la taille maximum des collones */
#define MAX_SIZE_L 10 /* Défini la taille maximum des lignes */
#define MAX_SIZE_V 100 /* Défini la taille maximums des tableaux à deux dimensions - 100 car V = C*L */


/* Intégration de la fonction introduction) */
void Introduction(void);
void Introduction_fonction_classique(void);
void Introduction_fonction_tableau(void);
void menu3(int ex);
void menu4(int ex);
void menu5(int ex); /* correspond à la série 4.2 mais comme n'accepte pas les virgule j'ai mis menu5 */

/* Intégration des fonstions (série 3) */
void Tableau_complet(void);
void Tableau_sans_zeros(void);
void Tableau_inverse(void);
void Tableau_positif_et_tableau_negatif(void);

/* Intégration des fonstions (série 4.1) */
void Tableau_2_dimensions(void);
void Transformation_tableauM_en_tableauV(void);


/* Intégration des fonstions (série 4.2) */
void Calcul_polynome_de_degre_N(void);
void Insertion_valeur_dans_un_tableau_trie(void);
void Recherche_sequentielle(void);
void Recherche_dichotomique(void);
void Tri_par_selection_du_maximum(void);
void Tri_par_propagation(void);


#endif /* fonctionnalites_tab_h */

