//
//  fonctionnalites.c
//  Projet_Serie_1_2_3_4_Liliana
//
//  Created by Liliana Afonso on 16.11.18.
//  Copyright © 2018 Liliana Afonso. All rights reserved.
//

#include "fonctionnalites.h"

/* INTRODUCTION REPETEE ======================================================================= */

void Introduction(){
    
    /* Déclarations des variables */
    int type; // choix du type de fonction : Fonctions classiques ou Fonctions sur tableau
    
    /* Affichage de resélection d'un nouvel exercice */
    printf("\n\nQuel type de fonction souhaitez-vous exercer? \n\n");
    
    /* Saisie contrôlée de la série au clavier */
    do{
        printf("Tapez 1 pour avoir accès aux séries des fonctions classiques\nTapez 2 pour avoir accès aux séries des fonctions sur tableau\nTapez 0 pour quitter: ") ;
        scanf("%d",&type);
    } while (type !=0 && type !=1 && type!=2);
    
    /* Affiche l'introduction suivante correspondant au choix */
    switch (type) {
        case 0:
            printf("\nMerci et à bientôt! \n");  exit (1); /* break fonctionne que pour le premier tour de l'introduction c'est pour ça que j'ai utilisé exit (1) */
        case 1:
            Introduction_fonction_classique(); Introduction();
        case 2:
            Introduction_fonction_tableau(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}


/* INTRODUCTION AVEC LES FONCTIONS CLASSIQUES REPETEE =============================================================== */

void Introduction_fonction_classique(){

    /* Déclarations des variables */
    int serie; // choix de la série (série 1, série 2)
    int ex; // correspond au numéro de l'exercice choisi

    /* Affichage du choix de la série */
    printf("\n\nQuelle série souhaitez-vous?\n\n");

    /* Saisie contrôlée de la série au clavier */
    do{
        printf("Tapez 1 pour avoir accès à la série 1\nTapez 2 pour avoir accès à la série 2\nTapez 0 pour quitter: ") ;
        scanf("%d",&serie);
    } while (serie !=1 && serie!=2 && serie!=0);
    
    /* Condition suite au choix de la série */
    if (serie ==0) {
        printf("\nMerci et à bientôt! \n");
        exit (1);
    }
    else if (serie==1) {
        printf("\nVoici la liste d'exercices de la série %d:\n",serie);
        printf("1. ABC\n2. Division\n3. Résistance\n4. Aire triangle\n5. Somme avec 5 variables\n6. Somme avec 2 variables\n7. Calcul prix TTC\n8. Calcul prix net\n9. Calcul distance\n");
    }
    else if (serie ==2)
    {
         printf("\nVoici la liste d'exercices de la série %d:\n",serie);
        printf("1. Somme/Produit/Moyenne - méthode while\n2. Somme/Produit/Moyenne - méthode do while\n3. Somme/Produit/Moyenne - méthode for\n4. Multipications successives\n5. Série harmonique\n6. Somme/Produit/Moyenne - plusieurs chiffres\n7. Calcul nombre lu à rebours - suite de chiffre\n8. Calcul nombre lu à rebours - nombre inversé\n9. Valeur numérique polynôme de degrés n\n10. Affichage triangle\n11. Table des produits\n");
    }
    
//    MESSAGE: j'ai bien compris ce que vous m'avez proposé de faire concernant le do while pour les exercices mais comme je souhaite afficher un message d'erreur en cas de mauvaise saisie j'ai souhaité garder mon anvcienne version mais je vous ai mis en commentaire la version avec do while
//
//       /* Saisie du numéro de l'exercie au clavier */
//        if (serie==1){
//        do {
//          printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
//          scanf("%d",&ex);
//        } while(ex<0 || ex>9);
//
//        } else if (serie==2){
//        do{
//            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
//            scanf("%d",&ex);
//        } while(ex<0 || ex>11);
//        }

    /* Saisie du numéro de l'exercie au clavier */
    printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
    scanf("%d",&ex);

    /* l'utilisateur est obligé de choisir un exercice de la liste pour l'effectuer */
    if (serie==1){
        if (ex<0 || ex>9){
            printf("\nCeci ne correspond à aucun exercice de la serie %d",serie);
            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
            scanf("%d",&ex);
        }
    }

    if (serie==2){
        if (ex <=0 || ex>11){
            printf("\nCeci ne correspond à aucun exercice de la serie %d",serie);
            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = page d'accueil): ");
            scanf("%d",&ex);
        }
    }

    /* Exécute les exercices de la série 1 */
    if (serie==1) {
        menu1(ex);
        /* Exécute les exercices de la série 2 */
    } else if (serie==2){
        menu2(ex);
    }
}


void menu1(int ex){
    /* Affiche l'exercice correspondant au choix */
    switch (ex) {
        case 0:
            printf("\nMerci et à bientôt!\n");  exit (1); /* break fonctionne que pour le premier tour de l'introduction c'est pour ça que j'ai utilisé exit (1) */
        case 1:
            ABC(); Introduction(); /* Introduction = Proposition de choisir un nouvel exercice donc pas de break */
        case 2:
            Division(); Introduction();
        case 3:
            Resistance(); Introduction();
        case 4:
            Aire_triangle(); Introduction();
        case 5 :
            Somme_avec_5_variables(); Introduction();
        case 6:
            Somme_avec_2_variables(); Introduction();
        case 7:
            Calcul_prix_TTC(); Introduction();
        case 8:
            Calcul_prix_net(); Introduction();
        case 9:
            Calcul_distance(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}

void menu2(int ex){
    /* Affiche l'exercice correspondant au choix */
    switch (ex) {
        case 0:
            printf("\nMerci et à bientôt! \n"); break;
        case 1:
            Somme_Produit_Moyenne_methode_while(); Introduction();
        case 2:
            Somme_Produit_Moyenne_methode_DoWhile(); Introduction();
        case 3:
            Somme_Produit_Moyenne_methode_for(); Introduction();
        case 4:
            Multipications_successives(); Introduction();
        case 5:
            Serie_harmonique(); Introduction();
        case 6:
            Somme_Produit_Moyenne_plusieurs_chiffres(); Introduction();
        case 7:
            Calcul_nombre_lu_a_rebours_suite_de_chiffre(); Introduction();
        case 8:
            Calcul_nombre_lu_a_rebours_nombre_inverse(); Introduction();
        case 9:
            Valeur_numerique_polynome_de_degres_n(); Introduction();
        case 10:
            Affichage_triangle(); Introduction();
        case 11:
            Table_des_produits(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}

/* EXERCICES DE LA SERIE 1 ======================================================================= */

void ABC() {

    /* Déclarations des variables */
    int a;
    int b;
    int c;
    int temp;

    /* Annonce de l'exercice */
    printf("\nABC / Exercice 4 - Série 1\n\n");

    /* Saisie de a au clavier */
    printf("entrez une valeur a: ");
    scanf("%d",&a);

    /* Saisie de b au clavier */
    printf("entrez une valeur b: ");
    scanf("%d",&b);

    /* Saisie de c au clavier */
    printf("entrez une valeur c: ");
    scanf("%d",&c);

    /* Valeurs des déclarations*/
    temp = a;
    a = b;
    b = c;
    c = temp;

    /* Affichage des resultats */
    printf("\nla valeur de a: %d.\n",a);
    printf("la valeur de b: %d.\n",b);
    printf("la valeur de c: %d.\n",c);
}

void Division(){

    /* Déclarations des variables */
    int N;
    int M;
    float DIVISION;
    float RESTE;

    /* Annonce de l'exercice */
    printf("\nDivision / Exercice 5 - Série 1\n\n");

    /* Saisie de N au clavier */
    printf("Introduisez un nombre entier :" );
    scanf("%d",&N);

    /* Saisie de M au clavier */
    printf("Introduisez un autre nombre entier :");
    scanf("%d",&M);

    /* Indications des formules */
    DIVISION = (float)N/M;
    RESTE = DIVISION - (M%N);

    /* Affichage des resultats */
    printf("La division de %d et de %d",N,M);
    printf(" est de %f.\n", DIVISION);
    printf("La division entière est de %d.\n", M%N);
    printf("Le reste de la division entière est de %f.\n",RESTE);
}

void Resistance(){

    /* Déclarations des variables */
    double R1, R2, R3;
    double Serie;
    double Parallele;
    int Rep; // Correspond à la réponse de la première question

    /* Annonce de l'exercice */
    printf("\nResistance / Exercice 6 - Série 1\n\n");

    /* Saisie de Rep au clavier avec l'obligation de répondre soit par 1 soit par 0 */
    do{
        printf("Est-ce que les résistances sont-elles branchées en série? (oui = 1 | non = 0)");
        scanf("%d",&Rep);
    } while (Rep !=0 && Rep !=1);

    /* Saisie de R1 R2 et R3 au clavier */
    printf("Entrez les trois valeurs une à la fois:\n");
    scanf("%lf", &R1);
    scanf("%lf", &R2);
    scanf("%lf", &R3);

    /* Indications des formules */
    Serie = R1 + R2 + R3;
    Parallele = ((R1*R2*R3)/(R1*R2+R1*R3+R2*R3));

    /* Affichage des resultats selon la méthode choisie */
    if (Rep ==1){
        printf("Le résultat est: %.2lf",Serie);
    }else {
        printf("Le résultat est: %.2lf",Parallele);
    }
}

void Aire_triangle(){

    /* Déclarations des variables */
    int A;
    int B;
    int C;
    float Aire;
    float P ; // demi-périmètre

    /* Annonce de l'exercice */
    printf("\nAire triangle / Exercice 7 - Série 1\n\n");

    /* Saisie de A au clavier */
    printf("Quelle est la valeur d'un côté ? (nombre entier)\n");
    scanf("%d",&A);

    /* Saisie de B au clavier */
    printf("Quelle est la valeur du second côté ? (nombre entier)\n" );
    scanf("%d",&B);

    /* Saisie de C au clavier */
    printf("Quelle est la valeur du troisième côté ? (nombre entier)\n");
    scanf("%d",&C);

    /* Indications des formules */
    P = ((float)((A+B+C)/2));
    Aire = (P*((P-A)*(P-B)*(P-C)));

    /* Affichage des resultats */
    printf("voici le résultat de l'aire: %.2f \n",Aire);
    printf("Voici le résultat du demi-périmètre: %.2f \n",P);
}

void Somme_avec_5_variables(){

    /* Déclarations des variables */
    int A;
    int B;
    int C;
    int D;
    int Somme;

    /* Annonce de l'exercice */
    printf("\nSomme avec 5 variables / Exercice 8a - Série 1\n\n");

    /* Saisie de A au clavier */
    printf("Quelle est la valeur de A: ");
    scanf("%d",&A);

    /* Saisie de B au clavier */
    printf("Quelle est la valeur de B: ");
    scanf("%d",&B);

    /* Saisie de C au clavier */
    printf("Quelle est la valeur de C: ");
    scanf("%d",&C);

    /* Saisie de D au clavier */
    printf("Quelle est la valeur de D: ");
    scanf("%d",&D);

    /* Indications de la formule Somme */
    Somme = A+B+C+D;

    /* Affichage du resultat */
    printf("\nLa somme est égale à : %d",Somme);
}

void Somme_avec_2_variables(){

    /* Déclarations des variables */
    int  A;
    long SOMME;

    /* Annonce de l'exercice */
    printf("\nSomme avec 5 variables / Exercice 8a - Série 1\n\n");

    /* Valeur initiale de SOMME */
    SOMME = 0;

    /* Saisie de A premier au clavier */
    printf("Entrez le premier nombre : ");
    scanf("%d", &A);
    SOMME+=A; // va additionner la SOMME + la valeur A

    /* Saisie de A second au clavier */
    printf("Entrez le deuxième nombre : ");
    scanf("%d", &A);
    SOMME+=A;

    /* Saisie de A troisième au clavier */
    printf("Entrez le troisième nombre : ");
    scanf("%d", &A);
    SOMME+=A;

    /* Saisie de A quatrième au clavier */
    printf("Entrez le quatrième nombre : ");
    scanf("%d", &A);
    SOMME+=A;

    /* Affichage du resultat */
    printf("La somme des nombres entrés est %ld\n", SOMME);
}

void Calcul_prix_TTC(){

    /* Déclarations des variables */
    double Prix_TTC;
    int Prix_Net;
    int TVA;

    /* Annonce de l'exercice */
    printf("\nCalcul prix TTC / Exercice 9a - Série 1\n\n");

    /* Saisie du prix HT au clavier */
    printf("Quel est le prix HT de l'article? ");
    scanf("%d",&Prix_Net);

    /* Saisie de la TVA au clavier */
    printf("Quel est le pourcentage de TVA? ");
    scanf("%d",&TVA);

    /* Indications de la formule Prix TTC */
    Prix_TTC = (Prix_Net * ((double)TVA/100))+ Prix_Net;

    /* Affichage du resultat */
    printf("Le prix TTC de l'article est de CHF %.2lf!\n",Prix_TTC);
}

void Calcul_prix_net(){

    /* Déclarations des variables */
    double PNET; // Prix net
    double PTTC; // Prix TTC
    int TVA; //TVA en %

    /* Annonce de l'exercice */
    printf("\nCalcul prix net / Exercice 9b - Série 1\n\n");

    /* Saisie du prix TTC au clavier */
    printf("Quelle est la valeur de l'article TTC? ");
    scanf("%lf",&PTTC);

    /* Saisie de la TVA au clavier */
    printf("Quel est le pourcentage de la TVA? ");
    scanf("%d",&TVA);

    /* Indications de la formule PNET */
    PNET = (double)(PTTC * 100)/(100+TVA);

    /* Affichage du resultat */
    printf("Le prix net est de CHF %.2lf\n",PNET);
}

void Calcul_distance(){

    /* Déclarations des variables */
    double DIST;
    int XA;
    int XB;
    int YA;
    int YB;

    /* Annonce de l'exercice */
    printf("\nCalcul distance / Exercice 10 - Série 1\n\n");

    /* Saisie du point A(XA;YA) au clavier */
    printf("Quelles sont les cordonées du point A(XA;YA):");
    scanf("%d,%d",&XA,&YA);

    /* Saisie du point B(XB;YB) au clavier */
    printf("Quelles sont les cordonnées du point B(XB;YB):");
    scanf("%d,%d",&XB,&YB);

    /* Indications de la formule DIST */
    DIST = sqrt(pow(XB-XA,2)+pow(YB-YA,2));

    /* Affichage du resultat */
    printf("La distance de AB est de %.2lf.\n",DIST);
}

 /* EXERCICES DE LA SERIE 2 ======================================================================= */

void Somme_Produit_Moyenne_methode_while(){

    /* Déclarations des variables */
    int N; // c'est le nombre de nombre différents on va demander
    int A;
    int i; // on appelle ça le compteur (nombre de fois qu'on va demander l'information)
    long SOMME=0;
    long PRODUIT=1;
    double MOYENNE;

    /* Annonce de l'exercice */
    printf("\nSomme/Produit/Moyenne - méthode while / Exercice 1a - Série 2\n\n");

    /* Saisie de N au clavier */
    do{
        printf("Ecrivez le nombre de saisie:");
        scanf("%d",&N);
    }while(N<=0);

    /* Saisie des données + Traitement */
    i=0;

    /* Boucle While lors que A > 0 */
    while(i<N){

        do{
            printf("Ecrivez un nombre:");
            scanf("%d",&A);
        } while(A<=0); // c'est un do while dans une boucle while pour éviter que la personne introduise la valeur de 0

        /* Indications des formules */
        SOMME += A;
        PRODUIT *= A;
        i=i+1; // marque la fin
    }

    /* Indications des formules */
    MOYENNE = (double)SOMME/N; // toujours à l'extérieur des boucles car on va récolter la somme qui est dans la boucle

    /* Affichage des resultats */
    printf("la somme est: %ld\n",SOMME);
    printf("le produit est: %ld\n",PRODUIT);
    printf("la moyenne est: %lf\n",MOYENNE);
}

void Somme_Produit_Moyenne_methode_DoWhile(){

    /* Déclarations des variables */
    int N; // c'est le nombre de numéros différents qu'on va demander
    int A;
    int i;
    long SOMME=0;
    long PRODUIT=1;
    double MOYENNE;

    /* Annonce de l'exercice */
    printf("\nSomme/Produit/Moyenne - méthode do while / Exercice 1b - Série 2\n\n");

    /* Boucle do while: demande de saisir un nombre tant ce que celui-ci n'est pas suppérieur à 0 */
    do{
        /* Saisie de N au clavier */
        printf("Ecrivez le nombre de saisie:");
        scanf("%d",&N);
    }while(N<=0);

    /* Valeur de i au départ */
    i=1;

    /* Boucle do while : tant que le nombre de saisie est inférieur à N alors on redemande d'écrire un nombre */
    do{
        /* Saisie de A au clavier */
        printf("Ecrivez un nombre:");
        scanf("%d",&A);

        /* Indications des formules */
        SOMME += A;
        PRODUIT *= A;
        i++;

    } while(i<=N);

    /* Indications de la MOYENNE */
    MOYENNE = (double)SOMME/N;

    /* Affichage des resultats */
    printf("\nla somme est: %ld\n",SOMME);
    printf("le produit est: %ld\n",PRODUIT);
    printf("la moyenne est: %lf\n",MOYENNE);
}

void Somme_Produit_Moyenne_methode_for(){

    /* Déclarations des variables */
    int N; // c'est le nombre de nombre différents on va demander
    int A;
    int i;  // i = le nombre de fois qu'on va demander l'information
    long SOMME=0;
    long PRODUIT=1;
    double MOYENNE;

    /* Annonce de l'exercice */
    printf("\nSomme/Produit/Moyenne - méthode for / Exercice 1c - Série 2\n\n");

    /* Saisie de N au clavier */
    printf("Ecrivez le nombre de saisie ");
    scanf("%d",&N);

    /* rentre dans la boucle for jusqu'à ce que le nombre de saisie rentré >N */
    for (i=0; i<N; i=i+1) {
        /** Saisir des données + Traitement ???*/
        do {
            /* Saisie de A au clavier */
            printf("Ecrivez un nombre:");
            scanf("%d",&A); // puis on l'enrengistre dans A
        } while(A<=0); //

        /* Indications des formules */
        SOMME += A;
        PRODUIT *= A;
    }

    /* Indications de la formule Moyenne */
    MOYENNE = (double)SOMME/N;

    /* Affichage des resultats */
    printf("la somme est: %ld\n",SOMME);
    printf("le produit est: %ld\n",PRODUIT);
    printf("la moyenne est: %.2lf\n",MOYENNE);
}

void Multipications_successives(){

    /* Déclarations des variables */
    unsigned int X;
    unsigned int N;
    long PUISSANCE;

    /* Annonce de l'exercice */
    printf("\nMultipications successives / Exercice 2 - Série 2\n\n");

    /* Saisie de X au clavier */
    printf("Entrez la valeur de X : \n");
    scanf("%u",&X);

    /* Saisie de N au clavier */
    printf("Entrez la valeur de N : \n");
    scanf("%u",&N);

    /* Indications de la formule PUISSANCE */
    PUISSANCE = (long)pow(X,N);

    /* Affichage du resultat */
    printf(" %u à  la puissance %u vaut : %ld",X, N, PUISSANCE);
}

void Serie_harmonique(){

    /* Déclarations des variables */
    int N;
    int i = 1;
    double SUM=0;

    /* Annonce de l'exercice */
    printf("\nSérie harmonique / Exercice 3 - Série 2\n\n");

    /* Saisie de N au clavier */
    printf("Introduisez la valeur de N: \n");
    scanf("%d",&N);

    /* rentre dans la boucle for jusqu'à ce que le nombre de saisie rentré > N */
    for (i=1; i<=N; i++)
    {
        SUM+= (1.0/i);
    }

    /* Affichage du resultat */
    printf("Le résultat est de: %lf\n",SUM);
}

void Somme_Produit_Moyenne_plusieurs_chiffres(){

    /* Déclarations des variables */
    long SOMME= 0;
    long PRODUIT =1;
    double MOYENNE;
    int n;
    int i;

    /* Annonce de l'exercice */
    printf("\nSomme/Produit/Moyenne - plusieurs chiffres / Exercice 4 - Série 2\n\n");

    /* Valeur de départ de i */
    i=0;

    /* Saisie de n au clavier */
    printf("Entrez un nombre entre 1 et 9 (0 marque la fin):\n");
    scanf("%d",&n);

    /*tant que ce n'est pas 0 alors on rentre dans la boucle do while*/
    do
    {
        /* si sais autre que 1 à 9 alors bip sinon effectue calcul*/
        if (n>=10 || n<=-1 )
        {
            printf("BIIIIIIIIIIIIIIIIIP\n");
        }
        else
        {
            /* Indications des formules */
            SOMME+=n;
            PRODUIT *=n;
            i++; // compte le nombre de boucle faites
        }

        /* Saisie de n au clavier */
        printf("Entrez un nombre entre 1 et 9 (0 marque la fin):\n");
        scanf("%d",&n);

    }
    while (n!=0);

    /* Indications de la formule MOYENNE */
    MOYENNE = (double)SOMME/(i);

    /* Affichage des resultats */
    printf("la somme est: %ld\n",SOMME);
    printf("le produit est: %ld\n",PRODUIT);
    printf("la moyenne est: %.2lf\n",MOYENNE);
}

void Calcul_nombre_lu_a_rebours_suite_de_chiffre(){

    /* Déclarations des variables */
    char n;
    char slash;
    int i = 0;
    long s = 0;

    /* Annonce de l'exercice */
    printf("\nCalcul nombre lu à rebours - suite de chiffre / Exercice 5 - Série 2\n\n");

    /* boucle do while dès que n ≠ 0 */
    do{
        printf("Entrez un nombre (0 marque la fin): ");
        scanf("%c%c",&n,&slash);

        /* permet de ne pas prendre les valeurs <0 et >9 */
        if (n<'0' || n >'9')
        {
            printf("Veuillez écrire un chiffre!!! ");
        }
        else /*si entre 0 et 9 alors: ... */
        {
            s+= ((n - '0') * pow(10,i));
            i++;
        }
    } while (n!='0');

    /* Affichage du resultat */
    printf("SOMME :%ld",s);
}

void Calcul_nombre_lu_a_rebours_nombre_inverse(){

    /* Déclarations des variables */
    int n;
    int rem;
    int rev = 0;

    /* Annonce de l'exercice */
    printf("\nCalcul nombre lu à rebours - nombre inversé / Exercice 6 - Série 2\n\n");

    /* Saisie de n au clavier */
    printf("Entrer un nombre : ");
    scanf("%d", &n);

    /* While : fait la boucle lorsque n est supérieur à 0 */
    while (n>0) {
        rem = n % 10;
        rev = rev * 10 + rem;
        n = n / 10;
    }

    /* Affichage du resultat */
    printf("\nle révérser de ce nombre est: %d", rev);
}

void Valeur_numerique_polynome_de_degres_n(){

    /* Déclarations des variables */
    float X;
    int n; // exposant
    int A;
    double P; // Horner

    /* Annonce de l'exercice */
    printf("\nValeur numérique polynôme de degrés n / Exercice 7 - Série 2\n\n");

    /* Saisie de X au clavier */
    printf("Indroiduisez la valeur X:\n");
    scanf("%f",&X);

    /* Saisie de n au clavier */
    printf("Indroiduisez la valeur n:\n");
    scanf("%d",&n);

    /* Boucle For : va demander d'introduire A autant de fois que la va leur de n plus 1
     ex: n = 2 alors il sera demande A2 A1 et A0 */
    for(P=0 ; n>=0 ; n=n-1)
    {
        /* Saisie de A au clavier */
        printf("Intoduisez la valeur A%d : ",n);
        scanf("%d", &A);
        P = P*X + A;
    }

    /* Affichage du resultat */
    printf("Le résultat est: %.2lf",P);
}

void Affichage_triangle(){

    /* Déclarations des variables */
    int N;
    int i=0;
    int I; // compte les caractères
    int ESPACE; // représente l'espace avant de fair eune étoile

    /* Annonce de l'exercice */
    printf("\nAffichage triangle / Exercice 8 - Série 2\n\n");

    /* Saisie de N au clavier */
    printf("Entrez le nombre de ligne :");
    scanf("%d",&N);

    /* Boucle for : va se répété autant de fois que N */
    for (i=0; i<N; i++) // dit  combien de fois cette boucle doit se répéter
    {
        ESPACE = N-i-1; // important de le mettre dans la boucle

        for (I=0; I<ESPACE; I++) // va "déssiner" un espace avant de dessiner *
            printf(" ");

        for (I=0 ; I<2*i+1  ; I++) // dit combien de * il en faut (2 car il y a tjrs deux de plus pour agrandir le triangle)
            printf("*");

        putchar('\n'); // va à la ligne à chaque boucle
    }
}

void Table_des_produits(){
    
    /* Déclarations des variables */
    const int MAX = 10; /* nombre de lignes et de colonnes */
    int I; /* compteur des lignes   */
    int J; /* compteur des colonnes */
    
    /* Annonce de l'exercice */
    printf("\nTable des produits / Exercice 9 - Série 2\n\n");
        
    /* Affichage de l'en-tête */
    printf(" X*Y I");
    for (J=0 ; J<=MAX ; J++)
        printf("%4d", J);
    printf("\n");
    printf("------");
    
    for (J=0 ; J<=MAX ; J++)
        printf("----");
    printf("\n");
        
    /* Affichage du tableau */
    for (I=0 ; I<=MAX ; I++)
    {
        printf("%3d  I", I);
        for (J=0 ; J<=MAX ; J++)
            printf("%4d", I*J);
            printf("\n");
        }
}

