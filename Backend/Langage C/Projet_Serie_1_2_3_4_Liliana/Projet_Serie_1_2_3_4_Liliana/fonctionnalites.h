//
//  fonctionnalites.h
//  Projet_1_2_Liliana
//
//  Created by Liliana Afonso on 22.10.18.
//  Copyright © 2018 Liliana Afonso. All rights reserved.
//

#ifndef FONCTIONNALITES_H_INCLUDED
#define FONCTIONNALITES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_ITERATION 50 /* le maximum d iteration à appliquer */

/* Intégration de la fonction introduction) */
void Introduction(void);
void Introduction_fonction_classique(void);
void Introduction_fonction_tableau(void);
void menu1(int ex);
void menu2(int ex);


/* Intégration des fonstions (série 1) */
void ABC(void);
void Division(void);
void Resistance(void);
void Aire_triangle(void);
void Somme_avec_5_variables(void);
void Somme_avec_2_variables(void);
void Calcul_prix_TTC(void);
void Calcul_prix_net(void);
void Calcul_distance(void);

/* Intégration des fonstions (série 2) */
void Somme_Produit_Moyenne_methode_while(void);
void Somme_Produit_Moyenne_methode_DoWhile(void);
void Somme_Produit_Moyenne_methode_for(void);
void Multipications_successives(void);
void Serie_harmonique(void);
void Somme_Produit_Moyenne_plusieurs_chiffres(void);
void Calcul_nombre_lu_a_rebours_suite_de_chiffre(void);
void Calcul_nombre_lu_a_rebours_nombre_inverse(void);
void Valeur_numerique_polynome_de_degres_n(void);
void Affichage_triangle(void);
void Table_des_produits(void);


#endif /* fonctionnalites_h */
