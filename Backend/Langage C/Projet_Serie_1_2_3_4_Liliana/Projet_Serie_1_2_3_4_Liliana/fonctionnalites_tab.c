//
//  fonctionnalites_tab.c
//  Projet_Serie_1_2_3_4_Liliana
//
//  Created by Liliana Afonso on 16.11.18.
//  Copyright © 2018 Liliana Afonso. All rights reserved.
//

#include "fonctionnalites_tab.h"

/* INTRODUCTION AVEC LES FONCTIONS DES TABLEAUX  REPETEE =============================================================== */

void Introduction_fonction_tableau(){
    
    /* Déclarations des variables */
    int serie; // choix de la série (série 3, série 4 ou série 5 alias série 4.2)
    int ex; // correspond au numéro de l'exercice choisi
    
    /* Affichage du choix de la série */
    printf("\n\nQuelle série souhaitez-vous?\n\n");
    
    /* Saisie contrôlée de la série au clavier */
    do{
        printf("Tapez 3 pour avoir accès à la série 3\nTapez 4 pour avoir accès à la série 4\nTapez 5 pour avoir accès à la série 5\nTapez 0 pour quitter: ") ;
        scanf("%d",&serie);
    } while (serie !=3 && serie!=4 && serie!=5 && serie!=0);
    
    /* Condition suite au choix de la série */
    if (serie==0) {
        printf("\nMerci et à bientôt! \n");
        exit (1);
    }
    else if (serie==3) {
        printf("\nVoici la liste d'exercices de la série %d:\n",serie);
        printf("1. Tableau complet\n2. Tableau sans les valeurs zéros\n3. Tableau inversé\n4. Tableau positif et tableau négatif\n");
    }
    else if (serie==4)
    {
        printf("\nVoici la liste d'exercices de la série %d:\n",serie);
        printf("1. Tableau à deux dimensions\n2. Transformation d'un tableau M en tableau V\n");
    }
    else if (serie==5){
        printf("\nVoici la liste d'exercices de la série %d:\n",serie);
        printf("1. Calcul d'un polynôme de degré N\n2. Insertion d'une valeur dans un tableau trié\n3. Recherche séquentielle\n4. Recherche dichotomique\n5. Tri par sélection du maximum\n6. Tri par propagation\n");
    }
    
//    MESSAGE: j'ai bien compris ce que vous m'avez proposé de faire concernant le do while pour les exercices mais comme je souhaite afficher un message d'erreur en cas de mauvaise saisie j'ai souhaité garder mon anvcienne version mais je vous ai mis en commentaire la version avec do while
//
//       /* Saisie du numéro de l'exercie au clavier */
//        if (serie==3){
//        do {
//          printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
//          scanf("%d",&ex);
//        } while(ex<0 || ex>4);
//
//        } else if (serie==4){
//        do{
//            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
//            scanf("%d",&ex);
//        } while(ex<0 || ex>2);
//
//        } else if (serie==5){
//        do{
//            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
//            scanf("%d",&ex);
//        } while(ex<0 || ex>6);
//        }
    
    
    /* Saisie du numéro de l'exercie au clavier */
    printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
    scanf("%d",&ex);
    
    /* l'utilisateur est obligé de choisir un exercice de la liste pour l'effectuer */
    if (serie==3){
        if (ex <0 || ex>4){
            printf("\nCeci ne correspond à aucun exercice de la serie %d",serie);
            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
            scanf("%d",&ex);
        }
    }
    
    if (serie==4){
        if (ex <0 || ex>2){
            printf("\nCeci ne correspond à aucun exercice de la serie %d",serie);
            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
            scanf("%d",&ex);
        }
    }
    
    if (serie==5){
        if (ex <0 || ex>6){
            printf("\nCeci ne correspond à aucun exercice de la serie %d",serie);
            printf("\nTapez le nombre correspondant à l'exercice souhaité (0 = Exit): ");
            scanf("%d",&ex);
        }
    }

        /* Exécute les exercices de la série 3 */
    if (serie==3){
        menu3(ex);
        /* Exécute les exercices de la série 4 */
    } else if (serie==4) {
        menu4(ex);
        /* Exécute les exercices de la série 4.2 */
    } else if (serie==5) {
        menu5(ex);
    }
}

void menu3(int ex){
    /* Affiche l'exercice correspondant au choix */
    switch (ex) {
        case 0:
            printf("\nMerci et à bientôt!\n");  exit (1); /* break fonctionne que pour le premier tour de l'introduction c'est pour ça que j'ai utilisé exit (1) */
        case 1:
            Tableau_complet(); Introduction();
        case 2:
            Tableau_sans_zeros(); Introduction();
        case 3:
            Tableau_inverse(); Introduction();
        case 4:
            Tableau_positif_et_tableau_negatif(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}

void menu4(int ex){
    /* Affiche l'exercice correspondant au choix */
    switch (ex) {
        case 0:
            printf("\nMerci et à bientôt! \n"); exit (1);
        case 1:
            Tableau_2_dimensions(); Introduction();
        case 2:
            Transformation_tableauM_en_tableauV(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}

void menu5(int ex){
    /* Affiche l'exercice correspondant au choix */
    switch (ex) {
        case 0:
            printf("\nMerci et à bientôt! \n"); exit (1);
        case 1:
            Calcul_polynome_de_degre_N(); Introduction();
        case 2:
            Insertion_valeur_dans_un_tableau_trie(); Introduction();
        case 3:
            Recherche_sequentielle(); Introduction();
        case 4:
            Recherche_dichotomique(); Introduction();
        case 5:
            Tri_par_selection_du_maximum(); Introduction();
        case 6:
            Tri_par_propagation(); Introduction();
        default:
            printf("Erreur! \n"); break; /* Si saisie inconnue alors programme fini */
    }
}

/* EXERCICES DE LA SERIE 3 ======================================================================= */

void Tableau_complet(void){
    
    /* Déclarations des variables */
    int N; /* taille du tableau */
    int i ; /* compteur */
    int T [MAX_SIZE_TAB]; /* Tableau */
    long SOM=0; /* Somme */

    
    /* Annonce de l'exercice */
    printf("\nTableau complet / Exercice 1 - Série 3\n\n");
    
    /* Saisie contrôlée de N au clavier */
    do {
        printf("Entrez le nombre d'éléments que vous voulez entrer: ");
        scanf("%d",&N);
    } while (N<0 || N>MAX_SIZE_TAB);
    
    /* Saisie au clavier de N valeur dans le tableau */
    for (i=0; i<N; i++)
        do {
            printf("Entrez la valeur %d: ",i+1);
            scanf("%d", &T[i]);
        } while (T[i] <0);
    
    /* Affichage du tableau */
    printf("Tableau donné:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    
    /* Somme des valeurs du tableau */
    for(i=0, SOM=0; i<N;i++)
    {
        SOM +=T[i];
    }
    
    /* Affichage du résultat */
    printf("\nSomme des éléments : %ld\n", SOM);
}

void Tableau_sans_zeros(void){
    
    /* Déclarations des variables */
    int N; /* taille du tableau */
    int i ; /* premier compteur */
    int j; /* deuxième compteur */
    int T [MAX_SIZE_TAB]; /* Tableau */
    
    /* Annonce de l'exercice */
    printf("\nTableau sans les valeurs zéros / Exercice 2 - Série 3\n\n");
    
    /* Saisie controlée de N au clavier */
    do {
        printf("Entrez le nombre d'éléments que vous voulez entrer: ");
        scanf("%d",&N);
    } while (N<0 || N>MAX_SIZE_TAB);
    
    /* Saisie au clavier de N valeur dans le tableau */
    for (i=0; i<N; i++)
        do {
            printf("Entrez la valeur %d: ",i+1);
            scanf("%d", &T[i]);
        } while (T[i] <0);
    
    /* Affichage du tableau */
    printf("Tableau donné:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    
    /* Suppresion des zero dans le tableau*/
    for (i=0; i<N; i++)
    {
        if(T[i]==0){
            for (j=i; j<N-1; j++){
                T[j] = T[j+1];
            } N--;
        }
    }
    
    /* Affichage du tableau sans les zéros*/
    printf("\nTableau donné sans les zéros:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    printf("\n");
}

void Tableau_inverse(void){
    
    /* Déclaration des variables */
    int N; /* taille du tableau */
    int i; /* compteur depuis le début*/
    int j; /* compteur depuis la fin */
    int T [MAX_SIZE_TAB]; /* tableau */
    int TEMP; /* variable pour la permutation*/
    
    /* Annonce de l'exercice */
    printf("\nTableau inversé / Exercice 3 - Série 3\n\n");
    
    /* Saisie controlée de N au clavier */
    do {
        printf("Entrez le nombre d'éléments que vous voulez entrer: ");
        scanf("%d",&N);
    } while (N<0 || N>MAX_SIZE_TAB);
    
    /* Saisie au clavier de N valeur dans le tableau */
    for (i=0; i<N; i++)
        do {
            printf("Entrez la valeur %d ",i+1);
            scanf("%d", &T[i]);
        } while (T[i] <0);
    
    /* Affichage du tableau */
    printf("\nTableau donné:\n");
    for (i=0; i<N; i++)
        printf("%4d", T[i]);
    
    /* Inversion de l'ordre des données entrées dans le tableau */
    for (i=0, j=N-1 ; i<j ; i++,j--) /* i<j permet de trouver le milieu ET I++ = J'AVANCE ET J-- = JE RECULE*/
    /* changement de la valeur de i en j etc (cf: ex.4 serie 1)*/
    {
        TEMP = T[i];
        T[i] = T[j];
        T[j] = TEMP;
    }
    
    /* Affichage du tableau inversé */
    printf("\nTableau donné à l'envers:\n");
    for (i=0; i<N; i++)
        printf("%4d", T[i]);
    printf("\n");
}

void Tableau_positif_et_tableau_negatif(void){
    
    /* Déclaration des variables */
    int N; /* Taille du tableau */
    int i = 0; /* Compteur nb total */
    int T [MAX_SIZE_TAB]; /* Tableau */
    int TPOS [MAX_SIZE_TAB]; /* TABLEAU DES NB POSITIFS */
    int TNEG [MAX_SIZE_TAB]; /* TABLEAU DES NB NEGATIFS */
    int POS=0; /* Compteur nb positifs */
    int NEG=0; /* Compteur nb négatifs */
    
    /* Annonce de l'exercice */
    printf("\nTableau positif et tableau négatif / Exercice 4 - Série 3\n\n");
    
    /* Saisie controlée de N au clavier */
    do {
        printf("Entrez le nombre d'éléments que vous voulez entrer: ");
        scanf("%d",&N);
    } while (N<0 || N>MAX_SIZE_TAB);
    
    /* Saisie au clavier de N valeur dans le tableau */
    for (i=0; i<N; i++) {
        printf("Entrez la valeur %d ",i+1);
        scanf("%d", &T[i]);}
    
    /* Affichage du tableau */
    printf("Tableau donné :\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    
    /* Séparation entre données positives et négatives */
    for (i=0; i<N; i++) /* parcours le tableau */
    {
        if(T[i]>=0) {
            {
                TPOS[POS] = T[i];
                POS++; /* pour acrémenter */
            }
        } else if (T[i]<0)
        {
            TNEG[NEG] = T[i];
            NEG++;
        }
    }
    
    /* Affichage du tableaux negatif */
    printf("\nTableau négatif donné :\n");
    for (i=0; i<NEG; i++)
        printf("%d ", TNEG[i]);
    printf("\n");
    
    /* Affichage du tableaux positif */
    printf("Tableau positif donné :\n");
    for (i=0; i<POS; i++)
        printf("%d ", TPOS[i]);
    printf("\n");
}

/* EXERCICES DE LA SERIE 4 ======================================================================= */

void Tableau_2_dimensions(void){
    
    /* Déclaration des variables */
    int C; /* nb de collones */
    int L; /* nb de ligne */
    int T [MAX_SIZE_C] [MAX_SIZE_L]; /* Tableau */
    int i =0; /* compteur de ligne */
    int j =0; /* compteur de collogne */
    long SOM; /* somme */
    
    /* Annonce de l'exercice */
    printf("\nTableau à deux dimensions / Exercice 1 - Série 4.1\n\n");
    
    /* Saisie controlée de C au clavier */
    do{
        printf("Entrez la taille de la collone du tableau: ");
        scanf("%d", &C);
    } while (C<0 || C > MAX_SIZE_C);
    
    /* Saisie controlée de L au clavier */
    do{
        printf("Entrez la taille de la ligne du tableau: ");
        scanf("%d", &L);
    } while (L<0 || L>MAX_SIZE_L);
    
    /* Saisie des valeurs du tableau */
    for (i=0; i<L; i++)
        for (j=0; j<C; j++)
        {
            printf("Valeur[%d][%d] : ",i,j);
            scanf("%d", &T[i][j]);
        }
    
    /* Affichage du tableau à deux dimensions */
    printf("Tableau donné :\n");
    for (i=0; i<L; i++)
    {
        for (j=0; j<C; j++)
            printf("%7d", T[i][j]); // %7d => le 7 marque l'espace si on veut un plus grand espace on met un plus grand chiffre
        printf("\n");
    }
    
    /* Calcul de la somme  */
    for (SOM=0, i=0; i<L; i++)
        for (j=0; j<C; j++)
            SOM += T[i][j];
    
    /* Affichage du de la somme */
    printf("Somme des éléments : %ld\n", SOM);
    
}

void Transformation_tableauM_en_tableauV(void){
    
    /* Déclaration des variables */
    int C; /* nb de collones */
    int L; // nb de ligne */
    int T [MAX_SIZE_C] [MAX_SIZE_L]; // tableau à deux dimensions */
    int i =0; // compteur de ligne */
    int j =0; // compteur de collone */
    int V [MAX_SIZE_V]; // tableau à une dimension */
    
    /* Annonce de l'exercice */
    printf("\nTransformation d'un tableau M en tableau V / Exercice 2 - Série 4.1\n\n");
    
    /* Saisie controlée de C au clavier */
    do{
        printf("Entrez la taille de la collone du tableau: ");
        scanf("%d", &C);
    } while (C<0 || C > MAX_SIZE_C);
    
    /* Saisie controlée de L au clavier */
    do{
        printf("Entrez la taille de la ligne du tableau: ");
        scanf("%d", &L);
    } while (L<0 || L>MAX_SIZE_L);
    
    /* Saisie des valeurs du tableau*/
    for (i=0; i<L; i++)
        for (j=0; j<C; j++)
        {
            printf("Valeur[%d][%d] : ",i,j);
            scanf("%d", &T[i][j]);
        }
    
    /* Affichage du tableau en M */
    printf("Tableau donné :\n");
    for (i=0; i<L; i++)
    {
        for (j=0; j<C; j++)
            printf("%3d", T[i][j]); // %3d => le 3 marque l'espace si on veut un plus grand espace on met un plus grand chiffre
        printf("\n");
    }
    
    /* Transfert des éléments ligne par ligne */
    for (i=0; i<L; i++)
        for (j=0; j<C; j++)
            V[i*C+j] = T[i][j];
    
    /* Affichage du tableau en V*/
    printf("Tableau en V :\n");
    for (i=0; i<(C*L); i++)
        printf("%3d ", V[i]);
    printf("\n");
    
}

/* EXERCICES DE LA SERIE 5 ALIAS 4.2 ======================================================================= */

void Calcul_polynome_de_degre_N(void) {

    /* Déclaration des variables */
    float X; /* Valeur X */
    int n;/* Valeur n */
    int i; /* Compteur */
    float T[MAX_SIZE_TAB]; /* Tableau */
    double P; /* Calcul Horner */
    
    /* Annonce de l'exercice */
    printf("\nCalcul d'un polynôme de degré N / Exercice 1 - Série 4.2\n\n");

    /* Saisie de X au clavier */
    printf("Quelle est la valeur de X: ");
    scanf("%f",&X);

    /* Saisie contrôlée de n au clavier */
    do{
        printf("Quelle est la valeur de n: ");
        scanf("%d",&n);
    }while(n<=0||(n+1)>MAX_SIZE_TAB);


    /* Remplissage & traitement du tableau A */
    for (i=0, P=0.0; i<n+1; i++){
        printf("Entrez la valeur A%d: ",i);
//        scanf("%f", &T[i]);
        
        //     P+=T[i]* pow(X, n-i); // calcul de P : version 1 - DEPART DEPUIS LE DEBUT
    }

    /* Calcul de P : version 2 - DEPART DEPUIS LA FIN */
    for (i=n,  P=0.0; i>=0; i--){
        P+=T[i]* pow(X, i);
    }

    /* Affiche tableau A */
    printf("Tableau A donné :\n");
    for (i=0; i<n+1; i++)
    printf("%.0f ", T[i]);
    printf("\n");

    /* Affichage de la valeur de P (résultat) */
    printf("P vaut: %.2f\n",P);
}

void Insertion_valeur_dans_un_tableau_trie(void){
    
    /* Déclation des variables */
    int T[MAX_SIZE_TAB]={7,15,30,42,50}; /* Tableau A*/
    int i=0; /* compteur i*/
    int N=5; /* Taille du tableau A */
    int VAL; /* Saisie au clavier */
    int index; /* Pour insérer VAL */
    
    /* Annonce de l'exercice */
    printf("\nInsertion d'une valeur dans un tableau trié / Exercice 2 - Série 4.2\n\n");
    
    /* Affiche le tableau A */
    printf("Tableau A:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
        printf("\n\n");

    /* Saisie de VAL */
    printf("Veuillez insérer une valeur dans le tableau: ");
    scanf("%d", &VAL);
    
    /* index prend la valeur de i lorsque VAL < à une valeur du tableau */
    for(i = 0; i < N; i++)
    {
        if (T[i] > VAL) {
           break;
        }
    }
    index = i;
    
    /* Déplacement des valeurs */
    for (i=N; i > index; i--) {
        T[i]= T[i-1];
    } T[index]= VAL;
    
    /* Affichage du tableau final */
    printf("\nTableau donné ordonné:\n");
    for (i=0; i<N+1; i++)
        printf("%d ", T[i]);
    printf("\n");
}

void Recherche_sequentielle(void){
    
    /* Déclaration des variables */
    int T[MAX_SIZE_TAB]={3,73,6,2,30,9,10,82,58}; /* Tableau A */
    int i=0; /* compteur */
    int N=9; /* Taille du tableau */
    int VAL; /* Valeur saisie au clavier */
    int POS; /* Position de VAL */
    
    /* Annonce de l'exercice */
    printf("\nRecherche sequentielle / Exercice 3a - Série 4.2\n\n");
    
    /* Affichage du tableau A */
    printf("Tableau A:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    printf("\n\n");
    
    /* Saisie de VAL */
    printf("Quelle valeur souhaitez-vous voir? ");
    scanf("%d", &VAL);
    
    /* Compare successivement les valeurs du tableau avec la valeur donnée. */
    for( i=0, POS=-1; i<N; i++){
        if (T[i]==VAL){
            POS=i;
            i=N; // condition de sortie (comme break)
        }
    /* Affichage si VAL ≠ à une valeur du tableau */
    }
    if(POS==-1){
        printf("\nDésolé cette valeur ne se trouve pas dans le tableau");
        printf("\n");
    }
    /* Sinon affichage de la position de VAL dans le tableau */
    else{
        printf("\nIl se trouve à la %dème place",POS+1);
        printf("\n");
    }
}

void Recherche_dichotomique(void){
    
    /* Déclaration des variables */
    int T[MAX_SIZE_TAB]; /* Tableau A */
    int i; /* Compteur i */
    int N; /* Taille du tableau */
    int VAL; /* Valeur saisie au clavier */
    int POS; /* Position de l'élément */
    int SUP; /* Prend la dernière valeur */
    int INF; /* Prend la première valeur */
    int MILIEU; /* Milieu du tableau */
    
    /* Annonce de l'exercice */
    printf("\nRecherche dichotomique / Exercice 3b - Série 4.2\n\n");
    
    /* Saisie contrôlée de la taille du tableau A au clavier */
    do
    {
        printf("introduire la taille de T :");
        scanf("%d",&N);
    }
    while((N<1)||(N>MAX_SIZE_TAB));
    
    /* Saisie contrôlée du remplissage du tableau au clavier */
    for (i=0 ; i< N; i++)
    {
        do
        {
            printf ("Quelle est la valeur %d (par ordre croissant): ", i+1) ;
            scanf ("%d",&T[i]) ;
        }
        while(T[i]<0 || T[i]<T[i-1]);
    }
    
    /* Affichage du tableau A */
    printf("Tableau A:\n");
    for (i=0; i<N; i++)
        printf("%d ", T[i]);
    printf("\n\n");
    
    /* Saisie de VAL au clavier */
    printf("Quelle valeur souhaitez-vous voir? ");
    scanf("%d", &VAL);
    
    /* Initialisation des variables */
    i=0;
    POS = -1;
    SUP= N-1;
    INF=0;
    
    /* Compare le nombre recherché à la valeur au milieu du tableau */
    while ((INF<=SUP) && (POS==-1))
    {
        MILIEU = ((SUP+INF)/2);
        if(VAL > T[MILIEU]) {
//            INF=MILIEU+1;
        }
        else if (VAL<T[MILIEU]){
            SUP=MILIEU-1;
        }
        else {
            POS=MILIEU;
        }
    }
    
    /* Affichage du résultat */
    if (POS==-1) {
        printf("Désolé cette valeur ne se trouve pas dans le tableau\n");
    } else {
        printf("\nIl se trouve à la %dème place\n",POS+1);
    }
}

void Tri_par_selection_du_maximum(void){
    
    /* Déclarations des variables */
    int I; /* Indice qui parcours le tableau A */
    int J; /* Deuxième indice qui parcours le tableau A */
    int A[MAX_SIZE_TAB]={77,66,22,11,44,55,33}; /* Tableau A */
    int N=7; /* Taille du tableau */
    int PMAX=0; /* Premier maximum à droite du tableau A */
    int tmp;  /* Valeur pour permuter */
    
    /* Annonce de l'exercice */
    printf("\nTri par sélection du maximum / Exercice 4 - Série 4.2\n\n");
    
    /* Affichage du tableau A */
    printf("Tableau A:\n");
    for (I=0; I<N; I++)
        printf("%d ", A[I]);
    printf("\n");
    
    /* Parcoure le tableau de gauche à droite à l'aide de l'indice I et déterminer la position PMAX */
    for(I=0; I<N-1; I++){
        for (J=I+1, PMAX=J; J<N; J++)
        {
            /* Pour trouver le max */
            if(A[J]>A[PMAX])
            {
                PMAX = J;
            }
        }
        
        if(A[I] < A[PMAX])
        {
            /* Permutation */
            tmp = A[I];
            A[I] = A[PMAX];
            A[PMAX]= tmp;
        }
    }
    
    /* Affichage du tableau A décroissant */
    printf("\nTableau A décroissant:\n");
    for (I=0; I<N; I++)
        printf("%d ", A[I]);
    printf("\n");
}

void Tri_par_propagation(void){
    
    /* Déclarations des variables */
    int I; /* Indice qui parcours le tableau A */
    int J; /* Deuxième indice qui parcours le tableau A */
    int A[MAX_SIZE_TAB]={77,66,22,11,44,55,33}; /* Tableau A */
    int N=7; /* Taille du tableau */
    int tmp; /* Valeur pour permuter */
    
    /* Annonce de l'exercice */
    printf("\nTri par propagation / Exercice 5 - Série 4.2\n\n");
    
    /* Affichage du tableau A */
    printf("Tableau A:\n");
    for (I=0; I<N; I++)
        printf("%d ", A[I]);
    printf("\n");
    
    /* Propage, par permutations successives, le plus grand élément du tableau vers la fin du tableau */
    for(I=0; I<N-1; I++)
    {
        for (J=I+1 ; J<N; J++)
        {
            if (A[J]<A[I])
            {
                /* Permutation */
                tmp=A[I];
                A[I]=A[J];
                A[J]=tmp;
            }
        }
    }
    
    /* Affichage du tableau A ordonné A */
    printf("\nTableau A ordonné:\n");
    for (I=0; I<N; I++)
        printf("%d ", A[I]);
    printf("\n");
}

