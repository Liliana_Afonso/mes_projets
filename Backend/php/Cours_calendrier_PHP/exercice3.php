<?php

// Mois courant passé par paramètre
if ( isset( $_REQUEST['month'] ) )
{
	$current_month = (int)$_REQUEST['month'];
}
// Mois enregistré en cookie 
elseif ( isset( $_COOKIE['current_month'] ) )
{
	$current_month = (int)$_COOKIE['current_month'];
}
else
{
	$current_month = date( 'n' );
}

// Année courante passé par paramètre
if ( isset( $_REQUEST['year'] ) ) // REQUEST C'EST COMME GET    
{
	$current_year = (int)$_REQUEST['year'];
} 
// Annnée enregistrée en cookie 
elseif ( isset( $_COOKIE['current_year'] ) )
{
	$current_year = (int)$_COOKIE['current_year'];
}
else
{
	$current_year = date( 'Y' );
}

// Enregistrement en cookies
setcookie( 'current_month', $current_month, time()+60*60*24*30 ); // 30j en secondes 
setcookie( 'current_year', $current_year, time()+60*60*24*30 ); // 30j en secondes 

?>	
<html lang="en" class="">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<style class="cp-pen-styles" type="text/css">
	* {
		-webkit-font-smoothing: antialiased;
	}

	body {
		font-family: 'helvetica neue';
		background-color: #A25200;
		margin: 0;
	}

	.wrapp {
		width: 450px;
		margin: 30px auto;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
		align-content: center;
		align-items: center;
		box-shadow: 0 0 10px rgba(54, 27, 0, 0.5);
	}

	.flex-calendar .days,.flex-calendar .days .day.selected,.flex-calendar .month,.flex-calendar .week{
		display:-webkit-box;
		display:-webkit-flex;
		display:-ms-flexbox;
	}
	.flex-calendar{
		width:100%;
		min-height:50px;
		color:#FFF;
		font-weight:200
	}
	.flex-calendar .month {
		position:relative;
		display:flex;
		flex-direction:row;
		flex-wrap: nowrap;
		-webkit-justify-content:space-between;
				justify-content:space-between;
		align-content:flex-start;
		align-items:flex-start;
		background-color:#ffb835;
	}
	
	.flex-calendar .month .arrow,.flex-calendar .month .label {
		height:60px;
		order:0;
		flex:0 1 auto;
		align-self:auto;
		line-height:60px;
		font-size:20px;
	}
	
	.flex-calendar .month .arrow {
		width:50px;
		box-sizing:border-box;
		background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABqUlEQVR4Xt3b0U3EMBCE4XEFUAolHB0clUFHUAJ0cldBkKUgnRDh7PWsd9Z5Tpz8nyxFspOCJMe2bU8AXgG8lFIurMcurIE8x9nj3wE8AvgE8MxCkAf4Ff/jTEOQBjiIpyLIAtyJpyFIAjTGUxDkADrjhxGkAIzxQwgyAIPxZgQJAFJ8RbgCOJVS6muy6QgHiIyvQqEA0fGhAArxYQAq8SEASvHTAdTipwIoxk8DUI2fAqAc7w6gHu8KkCHeDSBLvAtApng6QLZ4KkDGeBpA1ngKQOb4YYDs8UMAK8SbAVaJNwGsFN8NsFq8FeADwEPTmvPxSXV/v25xNy9fD97v8PLuVeF9FiyD0A1QKVdCMAGshGAGWAVhCGAFhGGA7AgUgMwINICsCFSAjAh0gGwILgCZENwAsiC4AmRAcAdQR5gCoIwwDUAVYSqAIsJ0ADWEEAAlhDAAFYRQAAWEcIBoBAkAIsLX/rV48291MgAEhO747o0Rr82J23GNS+6meEkAw0wwx8sCdCAMxUsDNCAMx8sD/INAiU8B8AcCLT4NwA3CG4Az68/xOu43keZ+UGLOkN4AAAAASUVORK5CYII=) no-repeat;
		background-size:contain;
		background-origin:content-box;
		padding:15px 5px;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow:last-child {
		-webkit-transform:rotate(180deg);
			-ms-transform:rotate(180deg);
				transform:rotate(180deg);
	}
	
	.flex-calendar .month .arrow.visible {
		opacity:1;
		visibility:visible;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow.hidden {
		opacity:0;
		visibility:hidden;
		cursor:default;
	}
	
	.flex-calendar .days,.flex-calendar .week {
		line-height:25px;
		font-size:16px;
		display:flex;
		-webkit-flex-wrap: wrap;
				flex-wrap: wrap;
	}
	
	.flex-calendar .days {
		background-color:#FFF;
	}
	
	.flex-calendar .week {
		background-color:#faac1c;
	}
	
	.flex-calendar .days .day,.flex-calendar .week .day {
		flex-grow:0;
		-webkit-flex-basis: calc( 100% / 7 );
		min-width: calc( 100% / 7 );
		text-align:center;
	}
	
	.flex-calendar .days .day {
		min-height:60px;
		box-sizing:border-box;
		position:relative;
		line-height:60px;
		border-top:1px solid #FCFCFC;
		background-color:#fff;
		color:#8B8B8B;
		-webkit-transition:all .3s ease;
				transition:all .3s ease;
	}
	
	.flex-calendar .days .day.out {
		background-color:#fCFCFC;
	}
	
	.flex-calendar .days .day.disabled.today,.flex-calendar .days .day.today {
		color:#FFB835;
		border:1px solid;
	}
	
	.flex-calendar .days .day.selected {
		display:flex;
		flex-direction:row;
		flex-wrap:nowrap;
		-webkit-justify-content:center;
				justify-content:center;
		align-content:center;
		-webkit-align-items:center;
				align-items:center;
	}
	
	.flex-calendar .days .day.selected .number {
		width:40px;
		height:40px;
		background-color:#FFB835;
		border-radius:100%;
		line-height:40px;
		color:#FFF;
	}
	
	.flex-calendar .days .day:not(.disabled):not(.out) {
		cursor:pointer;
	}
	
	.flex-calendar .days .day.disabled {
		border:none;
	}
	
	.flex-calendar .days .day.disabled .number {
		background-color:#EFEFEF;
		background-image:url(data:image/gif;base64,R0lGODlhBQAFAOMAAP/14////93uHt3uHt3uHt3uHv///////////wAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAFAAUAAAQL0ACAzpG0YnonNxEAOw==);
	}
	
	.flex-calendar .days .day.event:before {
		content:"";
		width:6px;
		height:6px;
		border-radius:100%;
		background-color:#faac1c;
		position:absolute;
		bottom:10px;
		margin-left:-3px;
	}
	</style>

	<title>Calendar</title>
</head>

<body>
	<div class="wrapp">
		<div class="flex-calendar">
			
			<?php
				// Mois/année en cours			
				$this_month = strtotime( $current_year . '-' . $current_month );
			
				// Mois précédent - méthode 1
				if ( $current_month == 1 )
				{
					$previous_month = 12;
					$previous_year = $current_year - 1;
				}
				else
				{
					$previous_month = $current_month - 1;
					$previous_year = $current_year;
				}
				
				// Mois suivant - méthode 1
				if ( $current_month == 12 )
				{
					$next_month = 1;
					$next_year = $current_year + 1;
				}
				else
				{
					$next_month = $current_month + 1;
					$next_year = $current_year;
				}
				
				// Mois précédent - méthode 2
				$previous_month = date( 'm', strtotime( 'previous month', $this_month ) );
				$previous_year = date( 'Y', strtotime( 'previous month', $this_month ) );

				// Mois suivant - méthode 2
				$next_month = date( 'm', strtotime( 'next month', $this_month ) );
				$next_year = date( 'Y', strtotime( 'next month', $this_month ) );
				
			?>
			
			<div class="month">
				<a href="exercice3.php?year=<?php echo $previous_year ?>&month=<?php echo $previous_month ?>" class="arrow visible"></a>

				<div class="label">
					<?php echo date( 'F Y', $this_month ); ?>
				</div>

				<a href="exercice3.php?year=<?php echo $next_year ?>&month=<?php echo $next_month ?>" class="arrow visible"></a>
			</div>

			<div class="week">
				<div class="day">M</div>
				<div class="day">T</div>
				<div class="day">W</div>
				<div class="day">T</div>
				<div class="day">F</div>
				<div class="day">S</div>
				<div class="day">S</div>
			</div>

			<div class="days">
				
            <?php
            
 
				
				// Bornes du mois courant
				$first_day_of_month = date( 'N', strtotime( 'first day of ' . $current_year . '-' . $current_month ) ); 
				$last_day_of_month = date( 'd', strtotime( 'last day of ' . $current_year . '-' . $current_month ) ); // 30 ou 31 dans le mois (ou 28-29)
				
				$today = date( 'Y-n-d' );
				$disabled = array();
                $events = $_REQUEST['date'];

                // convertition de la date choisi par le client en format Y-n-d pour qu'il soit pareil que $i
                $date_event = new DateTime($_REQUEST['date']);
                $date_timestamp = $date_event->getTimestamp();
                $date_event_formate = date('Y-n-d', $date_timestamp) ;  

				// Décalage premier jour du mois
				for ( $i = 1; $i < $first_day_of_month; $i++ )
				{
					echo '<div class="day out"><div class="number"></div></div>';
				}
				
				// Calendrier
				for( $i = 1; $i <= $last_day_of_month; $i++ )
				{
					$classes = 'day';
					
					if ( ( $current_year . '-' . $current_month . '-' . $i ) == $today ) $classes .= ' selected'; // regarde le jour d'aujourd'hui 
					
					if ( in_array( $i, $disabled ) ) $classes .= ' disabled';

                    if ( ($current_year . '-' . $current_month . '-' . $i )== $date_event_formate)  $classes .= ' event';
					echo '<div class="' . $classes . '"><div class="number">' . $i . '</div></div>';
                }
            ?>
            

            </div>
           
        </div>

        <br>
        <hr>

        <!-- =======================PARTIE FORMULAIRE========================== -->
        <form method="get" action="exercice3.php" enctype = "multipart/form-data">
            <p>
                <label for="titreEvent">Titre de l'événement</label>
                <input type="text" name="titre" id="titre" value="">
            </p>
            <p>
                <label for="date">date</label>
                <input type="date" name="date" id="date" value="">
            </p>
            <p> 
                Veuillez sélectionner une image
                <input type="file" >  <!--- pour que le client puisse insérer un dossier-->
            </p>
            <p>
                <button type="submit">Envoyer</button>
            </p>
        </form>

        <?php
        if ( isset( $_REQUEST['titre'] ) && $_REQUEST['titre'] )
        echo '<p>Votre titre: ' . $_REQUEST['titre'] . '</p>';
        if ( isset( $_REQUEST['date'] ) && $_REQUEST['date'] )
        echo '<p>Votre date: ' . $_REQUEST['date'] . '</p>';

        $_FILES[file];
        if ( isset(  $_FILES['file'] ) &&  $_FILES['file'] )
        echo '<p>Votre image: ' .  $_FILES['file'] . '</p>';

        move_uploaded_file( $temp, $lilianaafonso/Desktop);

        if ( isset( $_FILES['monfichier'] ) && $_FILES['monfichier']['size'] )
        {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            if ( finfo_file($finfo, $_FILES['monfichier']['tmp_name']) == 'image/jpeg' )
            {
                move_uploaded_file( $_FILES['monfichier']['tmp_name'], 'hello.jpg' );
            }
        }

        // session 
        $_SESSION['nom'] = $_REQUEST['nom'];
        $_SESSION['date'] = $_REQUEST['date'];
        ?>

    </div>


        
</body>
</html>
