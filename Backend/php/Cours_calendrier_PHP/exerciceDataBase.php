<?php
// CONNEXION---------------------

try{
    $db = new PDO('mysql:host=localhost:8889;dbname=exercice_calendrier;charset=UTF8',
                'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (PDOException $e) {
    print "Erreur !:". $e->getMEssage()."<br/>";
}


// LECTURE -----------------------

$response = $db->query('SELECT id, date, titre, image_name, creator FROM events');
while ($info=$response->fetch(PDO::FETCH_ASSOC))
{
    echo '<pre>'; print_r($info);echo '</pre>';
}


// REQUETE PREPAREE (sert à contrôler les données entrées)---------------

$query = $db->prepare('SELECT id, date, titre, image_name, creator FROM events WHERE date = :var');
$date_fournie = '2019-03-12';
if ($reponse = $query->execute (array (':var'=> $date_fournie)))
{
    $info =$query->fetch(PDO::FETCH_ASSOC);
    echo '<pre>';print_r($infos); echo '</pre>';
}



// ECRITURE -----------------------

// FORME 1 :
$db->exec("INSERT INTO events (date, titre, image_name, creator) VALUES ('2018-04-25', 'Cours PHP', NULL, 1)");  // pas mettre id car auto incrémentée

// FORME 2 (la meilleure): 
$db->exec("INSERT INTO events SET date= '2019-04-06', titre = 'cours PHP du 06.04', image_name= NULL, creator= 1"); // pas mettre id car auto incrémentée


// MISE A JOUR  -----------------------

$db->exec("UPDATE events SET titre = 'cours PHP 2' WHERE date = '2019-04-06'"); // pour modifier

?>

