<?php

// on va prendre les données du fichier imdb.html
$var = file_get_contents ("https://www.imdb.com/title/tt6139732/");

// on veut le nom du titre du film
preg_match_all (('/<title>([\w\s]*) \(([\d]{4})\)/'), $var, $match);
print_r($match[1][0]); // print_r car c'est un tableau
echo "<br>" ;
print_r($match[2][0]);
echo '<br>';

// on veut la note du film
preg_match_all (('/itemprop="ratingValue">([\d]{1}\.[\d]{1})/'), $var, $match);
print_r($match[1][0]);
echo "<br>" ;

// on veut le noms des acteurs 
preg_match (('/"actor":[^\]]*/'), $var, $match); // on va prendre la partie où on parle des acteurs
preg_match_all (('/"name": "([\w\s]*)/'), $match[0], $match); // on va prendre dans la partie où on parle des acteurs les noms
print_r($match[1]);
echo "<br>" ;

// on veut l'affiche

preg_match_all (('/Poster"\nsrc="(.*)"/'), $var, $match);
print_r($match[1][0]);
echo "<img src=" .$match[1][0].">";
echo "<br>"; 


?>


