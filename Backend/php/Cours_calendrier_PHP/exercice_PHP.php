<?php 
echo "Hello world"; 
echo "<br>"  ;
echo date('d/m/Y H:i:s');
?>
<?php echo "<br>"  ;?>

<?php 
$starwars_movies = array('Un nouvel espoir', 'l\'Empire contre-attaque', 'Le retour du Jedi');
echo $starwars_movies[1];
?>

<?php echo "<br>"  ;?>

<?php
foreach ($starwars_movies as $array){ // array c'est le nom qu'on donne aux éléments
    echo 'film  : '.$array.'<br />'; }; 
?>

<?php 
$first_starwars_movie = array(
    'title' => 'Un nouvel espoir',
    'year' => 1977,
    'director' => 'George Lucas'
);

//Affiche "le premier film "Un nouvel espoir"
echo 'Le premier film s\'applle "'. $first_starwars_movie['title'].'",  réalisé par ' .$first_starwars_movie['director'] . ' et est sorti en ' .$first_starwars_movie['year'];

?>

<?php echo "<br>"  ;?>

<?php 
$starwars_movies_2 = array(
    array(
        'title' => 'Un nouvel espoir',
        'year' => 1977,
    ),
    array(
        'title' => 'L\'Empire contre-attaque',
        'year' => 1980,
    ),
    array(
        'title' => 'Le retour du Jedi',
        'year' => 1983,
    ),
);
?>
<?php echo "<br>"  ;?>

<?php
foreach ($starwars_movies_2 as $key =>  $array_2){ // array_2 c'est le nom qu'on donne aux éléments et key est une variable créé pour dire quel est le numéro du film
    echo 'N° ' .$key.' <br /> Titre du film: ' .$array_2['title']. '<br />Année du film: '.$array_2['year'].' <br />';
};
?>

<?php
$starwars_movies_3 = array ('Un nouvel espoir', 'L\Emire contre-attaque', 'Le retour du Jedi'); 

echo array_search('Le retour du Jedi', $starwars_movies_3); // valable pour tableau à une dimension

sort ($starwars_movies_3);// fonction ne retourne rien 

// ajout new élément à la fin (2 méthodes):
array_push($starwars_movies_3, 'Le reveil de la Force');// méthode 1 
$starwars_movies_3[] = 'Le reveil de la Force'; // méthode 2 (la plus utilisée car plus courte) si on met [0] ca va remplacer la valeur de 0 en cette new valeur

$line = implode(',', $starwars_movies_3); // ca va mettre des virgule entre les éléments (valable seulement pour tableau à une dimension)
?>

