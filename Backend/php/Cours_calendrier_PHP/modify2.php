<?php 

// CONNEXION à la DB---------------------

try{
    $db = new PDO('mysql:host=localhost:8889;dbname=exercice_calendrier;charset=UTF8',
                'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (PDOException $e) {
    print "Erreur !:". $e->getMEssage()."<br/>";
}

if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'update' && isset( $_REQUEST['id'] ) )
{
	$title = isset( $_REQUEST['title'] ) ? $_REQUEST['title'] : '';

	// Récupération des précédents événements
	$events = $_SESSION['events'];
	
	$new_event = array( 'title' => $title );
	
	// Ajout d'une image
	if ( isset( $_FILES['image'] ) && $_FILES['image']['size'] ) // ca aurait pu être photo à la place de image
	{
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
			
		if ( finfo_file($finfo, $_FILES['image']['tmp_name']) == 'image/jpeg' || 'image/png' ) // accepte les image en format jpeg ou png
		{
			move_uploaded_file( $_FILES['image']['tmp_name'], 'upload/' . $_FILES['image']['name'] );
			
			$new_event['image'] = $_FILES['image']['name'];
		}
	}

	// Ajout de l'événement dans un tableau qui contient toutes les dates
	$events[$_REQUEST['date']][] = $new_event;


    // Ecrire les données entrées dans la BD ----------------------- 
        $db->exec("UPDATE events SET titre = '".$_REQUEST['title']."',date = '".$_REQUEST['date']."',  image_name='".$new_event['image']."' WHERE id=".$_REQUEST['id']);
        

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modification</title>
</head>
<body>

	<form class="wrapp" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="update" />
		<p>
			<label for="date">Date</label>
			<input type="date" name="date" id="date" value="<?php echo date( 'Y-m-d' ) ?>" required />
		</p>
		<p>
			<label for="title">Titre</label>
			<input type="text" name="title" id="title" size="40" value="" />
		</p>
		<p>
			<label for="image">Image</label>
			<input type="file" name="image" id="image" />
		</p>
		<button type="submit">Valider</button>
	</form>

    </body>
</html>