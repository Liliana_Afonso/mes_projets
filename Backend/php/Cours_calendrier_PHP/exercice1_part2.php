<?php

// L'utilisateur est-il identifié?
if ( ! isset( $_COOKIE['user'] ) )
{
	// Si non, attribution d'une chaine aléatoire
	setcookie( 'user', rand(), time()+60*60*24*30 ); // 30j en secondes
}
else
{
	// Si oui, on prolonge de 30j le cookie
	setcookie( 'user', $_COOKIE['user'], time()+60*60*24*30 ); // 30j en secondes	
}

// Mois courant passé par paramètre
if ( isset( $_REQUEST['month'] ) )
{
	$current_month = (int)$_REQUEST['month'];
}
// Mois enregistré en cookie 
elseif ( isset( $_COOKIE['current_month'] ) )
{
	$current_month = (int)$_COOKIE['current_month'];
}
else
{
	$current_month = date( 'n' );
}

// Année courante passé par paramètre
if ( isset( $_REQUEST['year'] ) )
{
	$current_year = (int)$_REQUEST['year'];
} 
// Annnée enregistrée en cookie 
elseif ( isset( $_COOKIE['current_year'] ) )
{
	$current_year = (int)$_COOKIE['current_year'];
}
else
{
	$current_year = date( 'Y' );
}

// Enregistrement en cookies
setcookie( 'current_month', $current_month, time()+60*60*24*30 ); // 30j en secondes 
setcookie( 'current_year', $current_year, time()+60*60*24*30 ); // 30j en secondes 

// Connexion à la base de données
try{
	$db = new PDO('mysql:host=localhost:8889;dbname=coursphp;charset=utf8', 'root', 'root', array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ) );
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

// Récupération de l'action demandée
$action = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : false;

switch ( $action )
{
	// Enregistrement d'un événement
	case 'save':
		if ( isset( $_REQUEST['date'] ) )
		{
			$title = isset( $_REQUEST['title'] ) ? $_REQUEST['title'] : '';
			
			// Mise à jour de l'événement en base de données
			if ( isset( $_REQUEST['id'] ) && $_REQUEST['id'] )
			{
				$query = $db->prepare( 'UPDATE `event` SET `date` = :date, `title` = :title, `image_name` = :image WHERE `id` = :id AND `user` = :user' );
				$query->bindParam( ':id', $_REQUEST['id'], PDO::PARAM_INT );
			}
			// Nouvel événement en base de données
			else
			{
				$query = $db->prepare( 'INSERT INTO `event` SET `date` = :date, `title` = :title, `image_name`= :image , `user` = :user' );
			}
			
			$query->bindParam( ':user', $_COOKIE['user'], PDO::PARAM_INT );
			$query->bindParam( ':date', $_REQUEST['date'] );
            $query->bindParam( ':title', $_REQUEST['title'], PDO::PARAM_STR );
            $query->bindParam( ':image', $_FILES['image']['name'], PDO::PARAM_STR );
			$query->execute();
            
            // Ajout d'une image
	        if ( isset( $_FILES['image'] ) && $_FILES['image']['size'] ) // ca aurait pu être photo à la place de image
	        {
		        $finfo = finfo_open(FILEINFO_MIME_TYPE);		
		        if ( finfo_file($finfo, $_FILES['image']['tmp_name']) == 'image/png' ) // accepte les image en format png
		        {
			        move_uploaded_file( $_FILES['image']['tmp_name'], 'upload/' . $_FILES['image']['name'] );
            
                    // modification de l'image

					// Calcul des nouvelles dimensions
					$imsize = getimagesize('upload/' .$_FILES['image']['name']); // Va prendre les dimenssion de l'image
					$NewImgW=$ORGW= $imsize[0];
					$NewImgH=$ORGH= $imsize[1];
					
					// SI largueur > hauteur
					$width = 150;
					if (($ORGW>$ORGH) && ($ORGW> $width) )
					{
						$NewImgW = ($width * $ORGW) / $ORGH;
						$NewImgH = $width;
					}
					else if ($NewImgW > $width) 
					{
							$rognage = $NewImgW - $width; 
							$rognageCouper = $rognage/2;
						}
					
					$im = imagecreatefrompng('upload/' .$_FILES['image']['name']);

					$newImage = imagecreatetruecolor($NewImgW, $NewImgH);
					imageCopyResized($newImage,$im,0, $width, 0,0, $NewImgW,$NewImgH , $ORGW , $ORGH );
				
					imagefilter($im, IMG_FILTER_NEGATE);

					imagepng($im, 'upload/' . $_FILES['image']['name'].'-modifier.png', 9); // le format png n'accepte que entre 0 et 9 alors que jpeg entre 0 et 100
					imagepng($newImage, 'upload/' . $_FILES['image']['name'].'-redimensionnee.png', 9); // le format png n'accepte que entre 0 et 9 alors que jpeg entre 0 et 100
					
					imagedestroy($im);

			        $new_event['image'] = $_FILES['image']['name'];
		        }
			}
			
			header( 'Location:exercice1_part2.php' );
            exit();

        }

		break;
	
	// Modification d'un événement
	case 'edit':
		if ( isset( $_REQUEST['id'] ) )
		{
			$query = $db->prepare( 'SELECT * FROM `event` WHERE `id` = :id AND `user` = :user' );
			$query->bindParam( ':user', $_COOKIE['user'], PDO::PARAM_INT );
            $query->bindParam( ':id', $_REQUEST['id'], PDO::PARAM_INT);
			$query->execute();
			$current_event = $query->fetch( PDO::FETCH_ASSOC );
		}
		break;
	
	// Suppression d'un événement
	case 'delete':
		if ( isset( $_REQUEST['id'] ) )
		{
			$query = $db->prepare( 'DELETE FROM `event` WHERE `id` = :id AND `user` = :user' );
			$query->bindParam( ':user', $_COOKIE['user'], PDO::PARAM_INT );
            $query->bindParam( ':id', $_REQUEST['id'], PDO::PARAM_INT);
			$query->execute();
	
			header( 'Location:exercice1_part2.php' );
			exit();
		}
		break;
}

?>	
<html lang="en" class="">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<style class="cp-pen-styles" type="text/css">
	* {
		-webkit-font-smoothing: antialiased;
	}

	body {
		font-family: 'helvetica neue';
		background-color: #A25200;
		margin: 0;
	}

	.wrapp {
		width: 450px;
		margin: 30px auto;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
		align-content: center;
		align-items: center;
		box-shadow: 0 0 10px rgba(54, 27, 0, 0.5);
	}

	.flex-calendar .days,.flex-calendar .days .day.selected,.flex-calendar .month,.flex-calendar .week{
		display:-webkit-box;
		display:-webkit-flex;
		display:-ms-flexbox;
	}
	.flex-calendar{
		width:100%;
		min-height:50px;
		color:#FFF;
		font-weight:200
	}
	.flex-calendar .month {
		position:relative;
		display:flex;
		flex-direction:row;
		flex-wrap: nowrap;
		-webkit-justify-content:space-between;
				justify-content:space-between;
		align-content:flex-start;
		align-items:flex-start;
		background-color:#ffb835;
	}
	
	.flex-calendar .month .arrow,.flex-calendar .month .label {
		height:60px;
		order:0;
		flex:0 1 auto;
		align-self:auto;
		line-height:60px;
		font-size:20px;
	}
	
	.flex-calendar .month .arrow {
		width:50px;
		box-sizing:border-box;
		background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABqUlEQVR4Xt3b0U3EMBCE4XEFUAolHB0clUFHUAJ0cldBkKUgnRDh7PWsd9Z5Tpz8nyxFspOCJMe2bU8AXgG8lFIurMcurIE8x9nj3wE8AvgE8MxCkAf4Ff/jTEOQBjiIpyLIAtyJpyFIAjTGUxDkADrjhxGkAIzxQwgyAIPxZgQJAFJ8RbgCOJVS6muy6QgHiIyvQqEA0fGhAArxYQAq8SEASvHTAdTipwIoxk8DUI2fAqAc7w6gHu8KkCHeDSBLvAtApng6QLZ4KkDGeBpA1ngKQOb4YYDs8UMAK8SbAVaJNwGsFN8NsFq8FeADwEPTmvPxSXV/v25xNy9fD97v8PLuVeF9FiyD0A1QKVdCMAGshGAGWAVhCGAFhGGA7AgUgMwINICsCFSAjAh0gGwILgCZENwAsiC4AmRAcAdQR5gCoIwwDUAVYSqAIsJ0ADWEEAAlhDAAFYRQAAWEcIBoBAkAIsLX/rV48291MgAEhO747o0Rr82J23GNS+6meEkAw0wwx8sCdCAMxUsDNCAMx8sD/INAiU8B8AcCLT4NwA3CG4Az68/xOu43keZ+UGLOkN4AAAAASUVORK5CYII=) no-repeat;
		background-size:contain;
		background-origin:content-box;
		padding:15px 5px;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow:last-child {
		-webkit-transform:rotate(180deg);
			-ms-transform:rotate(180deg);
				transform:rotate(180deg);
	}
	
	.flex-calendar .month .arrow.visible {
		opacity:1;
		visibility:visible;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow.hidden {
		opacity:0;
		visibility:hidden;
		cursor:default;
	}
	
	.flex-calendar .days,.flex-calendar .week {
		line-height:25px;
		font-size:16px;
		display:flex;
		-webkit-flex-wrap: wrap;
				flex-wrap: wrap;
	}
	
	.flex-calendar .days {
		background-color:#FFF;
	}
	
	.flex-calendar .week {
		background-color:#faac1c;
	}
	
	.flex-calendar .days .day,.flex-calendar .week .day {
		flex-grow:0;
		-webkit-flex-basis: calc( 100% / 7 );
		min-width: calc( 100% / 7 );
		text-align:center;
	}
	
	.flex-calendar .days .day {
		min-height:60px;
		box-sizing:border-box;
		position:relative;
		line-height:60px;
		border-top:1px solid #FCFCFC;
		background-color:#fff;
		color:#8B8B8B;
		-webkit-transition:all .3s ease;
				transition:all .3s ease;
	}
	
	.flex-calendar .days .day.out {
		background-color:#fCFCFC;
	}
	
	.flex-calendar .days .day.disabled.today,.flex-calendar .days .day.today {
		color:#FFB835;
		border:1px solid;
	}
	
	.flex-calendar .days .day.selected {
		display:flex;
		flex-direction:row;
		flex-wrap:nowrap;
		-webkit-justify-content:center;
				justify-content:center;
		align-content:center;
		-webkit-align-items:center;
				align-items:center;
	}
	
	.flex-calendar .days .day.selected .number {
		width:40px;
		height:40px;
		background-color:#FFB835;
		border-radius:100%;
		line-height:40px;
		color:#FFF;
	}
	
	.flex-calendar .days .day:not(.disabled):not(.out) {
		cursor:pointer;
	}
	
	.flex-calendar .days .day.disabled {
		border:none;
	}
	
	.flex-calendar .days .day.disabled .number {
		background-color:#EFEFEF;
		background-image:url(data:image/gif;base64,R0lGODlhBQAFAOMAAP/14////93uHt3uHt3uHt3uHv///////////wAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAFAAUAAAQL0ACAzpG0YnonNxEAOw==);
	}
	
	.flex-calendar .days .day.event:before {
		content:"";
		width:6px;
		height:6px;
		border-radius:100%;
		background-color:#faac1c;
		position:absolute;
		bottom:10px;
		margin-left:-3px;
	}
	
	.flex-calendar .days .day .infos{
		padding: 5px 10px;
		position: absolute;
		left: 50%; top: 100%;
		-webkit-transform: translateX(-50%);
				transform: translateX(-50%);
		z-index: 1;
		background: #faac1c;
		color: white;
		font-size: 14px;
		font-weight: bold;
		line-height: normal;
		white-space: nowrap;
		opacity: 0;
		pointer-events: none;
		-webkit-transition:all .3s ease;
				transition:all .3s ease;
	}
	.flex-calendar .days .day:hover .infos{ opacity: 1 }
	
	form{
		padding: 20px;
		position: relative;
		background: white;
		box-sizing: border-box;
	}
	
	form p{ margin: 0 }
	form p + p{ margin-top: 20px }
	
	form label{ color: #8B8B8B }
	
	form input{
		height: 30px;
		font-size: 12px;
	}
	
	form button{
		padding: 10px 20px;
		position: absolute;
		right: 20px; bottom: 20px;
		background: #faac1c;
		border: none;
		color: white;
		font-size: 18px;
	}
	
	#events_list{
		padding: 20px;
		box-sizing: border-box;
		background: white;
		color: #8b8b8b
	}
	
	#events_list h2{ margin: 0; font-weight: normal }
	
	#events_list a{
		font-size: 12px;
		color: #faac1c;
		text-decoration: none;
	}
	#events_list a:hover{ text-decoration: underline }
	</style>

	<title>Calendar</title>
</head>

<body>
	<div class="wrapp">
		<div class="flex-calendar">
			
			<?php
				// Mois/année en cours			
				$this_month = strtotime( $current_year . '-' . $current_month );
			
				// Mois précédent - méthode 1
				if ( $current_month == 1 )
				{
					$previous_month = 12;
					$previous_year = $current_year - 1;
				}
				else
				{
					$previous_month = $current_month - 1;
					$previous_year = $current_year;
				}
				
				// Mois suivant - méthode 1
				if ( $current_month == 12 )
				{
					$next_month = 1;
					$next_year = $current_year + 1;
				}
				else
				{
					$next_month = $current_month + 1;
					$next_year = $current_year;
				}
				
				// Mois précédent - méthode 2
				$previous_month = date( 'm', strtotime( 'previous month', $this_month ) );
				$previous_year = date( 'Y', strtotime( 'previous month', $this_month ) );

				// Mois suivant - méthode 2
				$next_month = date( 'm', strtotime( 'next month', $this_month ) );
				$next_year = date( 'Y', strtotime( 'next month', $this_month ) );
				
			?>
			
			<div class="month">
				<a href="exercice1_part2.php?year=<?php echo $previous_year ?>&month=<?php echo $previous_month ?>" class="arrow visible"></a>

				<div class="label">
					<?php echo date( 'F Y', $this_month ); ?>
				</div>

				<a href="exercice1_part2.php?year=<?php echo $next_year ?>&month=<?php echo $next_month ?>" class="arrow visible"></a>
			</div>

			<div class="week">
				<div class="day">M</div>
				<div class="day">T</div>
				<div class="day">W</div>
				<div class="day">T</div>
				<div class="day">F</div>
				<div class="day">S</div>
				<div class="day">S</div>
			</div>

			<div class="days">
				
			<?php
				
				// Bornes du mois courant
				$first_day_of_month = date( 'N', strtotime( 'first day of ' . $current_year . '-' . $current_month ) );
				$last_day_of_month = date( 'd', strtotime( 'last day of ' . $current_year . '-' . $current_month ) );
				
				$today = new DateTime( 'today' );
				$disabled = array( new DateTime( '2018-05-21' ) );
				$events = array();
				
				// Récupération des événements en base de données et 
				// création d'un tableau regroupant les événements par jour
				$reponse = $db->query( 'SELECT * FROM `event` ORDER BY `date`, `id`' );
				while ( $event = $reponse->fetch( PDO::FETCH_ASSOC ) )
				{
					$events[$event['date']][] = $event;
				}
								
				// Décalage premier jour du mois
				for ( $i = 1; $i < $first_day_of_month; $i++ )
				{
					echo '<div class="day out"><div class="number"></div></div>';
				}
				
				// Calendrier
				for( $i = 1; $i <= $last_day_of_month; $i++ )
				{
					$infos = '';
					$classes = 'day';
					
					// Convertion du jour en cours en objet DateTime
					$current_day = new DateTime( $current_year . '-' . $current_month . '-' . $i );

					// Aujourd'hui?
					if ( $current_day == $today ) $classes .= ' selected';
					
					// Jour désactivé
					if ( in_array( $current_day, $disabled ) ) $classes .= ' disabled';
					
					// Jour avec événements?
					if ( isset( $events[$current_day->format( 'Y-m-d' )] ) ){
						$classes .= ' event';
						
						$event_text = '';
						foreach ( $events[$current_day->format( 'Y-m-d' )] as $event )
							$event_text .= $event['title'] . '<br />';
						
						$infos = '<div class="infos">' . $event_text . '</div>';
					}
					
					echo '<div class="' . $classes . '"><div class="number">' . $i . '</div>' . $infos . '</div>';
				}
			?>
			
			</div>
		</div>
	</div>
	
	<form class="wrapp" method="post" enctype="multipart/form-data"> <!-- important de mettre methode post en enctype pour les images !-->
		<input type="hidden" name="action" value="save" />
		<?php
			// Si modification, on ajout l'identifiant de l'événement
			if ( isset( $current_event ) ) 
				echo '<input type="hidden" name="id" value="' . $current_event['id'] . '" />';
		?>
		<p>
			<label for="date">Date</label>
			<input type="date" name="date" id="date" value="<?php if ( isset( $current_event ) ) echo $current_event['date'] ?>" required />
		</p>
		<p>
			<label for="title">Titre</label>
			<input type="text" name="title" id="title" size="40" value="<?php if ( isset( $current_event ) ) echo $current_event['title'] ?>" />
		</p>
        <p>
			<label for="image">Image</label>
			<input type="file" name="image" id="image" />
		</p>
		<button type="submit">Valider</button>
	</form>
	
	<div class="wrapp" id="events_list">
		<h2>Evénements du mois</h2>
		<ul>
			<?php
				// On vérifie qu'il y a au moins 1 événement
				if ( $events )
				{
					// On parcourt les jours
					foreach ( $events as $date => $day_events )
					{
						// On parcours les événements du jour
						foreach ( $day_events as $event )
						{
							$current_date = new DateTime( $date );
							
							echo '<li><em>' . $current_date->format( 'd.m.Y' ) . '</em> - ' . $event['title'];

							// L'événement appartient à l'utilisateur en cours?
							if ( $event['user'] == $_COOKIE['user'] )
								echo '&nbsp;<a href="exercice1_part2.php?action=edit&id=' . $event['id'] . '">Modifier</a>&nbsp;<a href="exercice1_part2.php?action=delete&id=' . $event['id'] . '" onclick="confirm(\'Supprimer cet événement?\')">Supprimer</a>';
							
							echo '</li>';
						}
                    }
                    // image
                    if ( isset( $_REQUEST['image'] ) ) {
						echo '<br/><img src="upload/' . $_REQUEST['image'] . '" width="50" />'; // important que l'image soit en png
					}
				}
			?>
		</ul>
	</div>
	
</body>
</html>