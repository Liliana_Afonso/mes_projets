<html lang="en" class="">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<style class="cp-pen-styles" type="text/css">
	* {
		-webkit-font-smoothing: antialiased;
	}

	body {
		font-family: 'helvetica neue';
		background-color: #A25200;
		margin: 0;
	}

	.wrapp {
		width: 450px;
		margin: 30px auto;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
		align-content: center;
		align-items: center;
		box-shadow: 0 0 10px rgba(54, 27, 0, 0.5);
	}

	.flex-calendar .days,.flex-calendar .days .day.selected,.flex-calendar .month,.flex-calendar .week{
		display:-webkit-box;
		display:-webkit-flex;
		display:-ms-flexbox;
	}
	.flex-calendar{
		width:100%;
		min-height:50px;
		color:#FFF;
		font-weight:200
	}
	.flex-calendar .month {
		position:relative;
		display:flex;
		flex-direction:row;
		flex-wrap: nowrap;
		-webkit-justify-content:space-between;
				justify-content:space-between;
		align-content:flex-start;
		align-items:flex-start;
		background-color:#ffb835;
	}
	
	.flex-calendar .month .arrow,.flex-calendar .month .label {
		height:60px;
		order:0;
		flex:0 1 auto;
		align-self:auto;
		line-height:60px;
		font-size:20px;
	}
	
	.flex-calendar .month .arrow {
		width:50px;
		box-sizing:border-box;
		background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABqUlEQVR4Xt3b0U3EMBCE4XEFUAolHB0clUFHUAJ0cldBkKUgnRDh7PWsd9Z5Tpz8nyxFspOCJMe2bU8AXgG8lFIurMcurIE8x9nj3wE8AvgE8MxCkAf4Ff/jTEOQBjiIpyLIAtyJpyFIAjTGUxDkADrjhxGkAIzxQwgyAIPxZgQJAFJ8RbgCOJVS6muy6QgHiIyvQqEA0fGhAArxYQAq8SEASvHTAdTipwIoxk8DUI2fAqAc7w6gHu8KkCHeDSBLvAtApng6QLZ4KkDGeBpA1ngKQOb4YYDs8UMAK8SbAVaJNwGsFN8NsFq8FeADwEPTmvPxSXV/v25xNy9fD97v8PLuVeF9FiyD0A1QKVdCMAGshGAGWAVhCGAFhGGA7AgUgMwINICsCFSAjAh0gGwILgCZENwAsiC4AmRAcAdQR5gCoIwwDUAVYSqAIsJ0ADWEEAAlhDAAFYRQAAWEcIBoBAkAIsLX/rV48291MgAEhO747o0Rr82J23GNS+6meEkAw0wwx8sCdCAMxUsDNCAMx8sD/INAiU8B8AcCLT4NwA3CG4Az68/xOu43keZ+UGLOkN4AAAAASUVORK5CYII=) no-repeat;
		background-size:contain;
		background-origin:content-box;
		padding:15px 5px;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow:last-child {
		-webkit-transform:rotate(180deg);
			-ms-transform:rotate(180deg);
				transform:rotate(180deg);
	}
	
	.flex-calendar .month .arrow.visible {
		opacity:1;
		visibility:visible;
		cursor:pointer;
	}
	
	.flex-calendar .month .arrow.hidden {
		opacity:0;
		visibility:hidden;
		cursor:default;
	}
	
	.flex-calendar .days,.flex-calendar .week {
		line-height:25px;
		font-size:16px;
		display:flex;
		-webkit-flex-wrap: wrap;
				flex-wrap: wrap;
	}
	
	.flex-calendar .days {
		background-color:#FFF;
	}
	
	.flex-calendar .week {
		background-color:#faac1c;
	}
	
	.flex-calendar .days .day,.flex-calendar .week .day {
		flex-grow:0;
		-webkit-flex-basis: calc( 100% / 7 );
		min-width: calc( 100% / 7 );
		text-align:center;
	}
	
	.flex-calendar .days .day {
		min-height:60px;
		box-sizing:border-box;
		position:relative;
		line-height:60px;
		border-top:1px solid #FCFCFC;
		background-color:#fff;
		color:#8B8B8B;
		-webkit-transition:all .3s ease;
				transition:all .3s ease;
	}
	
	.flex-calendar .days .day.out {
		background-color:#fCFCFC;
	}
	
	.flex-calendar .days .day.disabled.today,.flex-calendar .days .day.today {
		color:#FFB835;
		border:1px solid;
	}
	
	.flex-calendar .days .day.selected {
		display:flex;
		flex-direction:row;
		flex-wrap:nowrap;
		-webkit-justify-content:center;
				justify-content:center;
		align-content:center;
		-webkit-align-items:center;
				align-items:center;
	}
	
	.flex-calendar .days .day.selected .number {
		width:40px;
		height:40px;
		background-color:#FFB835;
		border-radius:100%;
		line-height:40px;
		color:#FFF;
	}
	
	.flex-calendar .days .day:not(.disabled):not(.out) {
		cursor:pointer;
	}
	
	.flex-calendar .days .day.disabled {
		border:none;
	}
	
	.flex-calendar .days .day.disabled .number {
		background-color:#EFEFEF;
		background-image:url(data:image/gif;base64,R0lGODlhBQAFAOMAAP/14////93uHt3uHt3uHt3uHv///////////wAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAFAAUAAAQL0ACAzpG0YnonNxEAOw==);
	}
	
	.flex-calendar .days .day.event:before {
		content:"";
		width:6px;
		height:6px;
		border-radius:100%;
		background-color:#faac1c;
		position:absolute;
		bottom:10px;
		margin-left:-3px;
	}
	</style>

	<title>Calendar</title>
</head>

<body>
	<div class="wrapp">
		<div class="flex-calendar">
			<div class="month">
				<div class="arrow visible"></div>

				<div class="label">
					MARCH 2019
				</div>

				<div class="arrow visible"></div>
			</div>

			<div class="week">
				<div class="day">M</div>
				<div class="day">T</div>
				<div class="day">W</div>
				<div class="day">T</div>
				<div class="day">F</div>
				<div class="day">S</div>
				<div class="day">S</div>
			</div>

			<div class="days">
				
				<div class="day out">
					<div class="number"></div>
				</div>

				<div class="day out">
					<div class="number"></div>
				</div>

				<div class="day out">
					<div class="number"></div>
				</div>

				<div class="day out">
					<div class="number"></div>
				</div>

				<div class="day">
					<div class="number">1</div>
				</div>

				<div class="day">
					<div class="number">2</div>
				</div>
				
				<div class="day">
					<div class="number">3</div>
				</div>

				<div class="day">
					<div class="number">4</div>
				</div>

				<div class="day">
					<div class="number">5</div>
				</div>

				<div class="day selected">
					<div class="number">6</div>
				</div>

				<div class="day">
					<div class="number">7</div>
				</div>

				<div class="day">
					<div class="number">8</div>
				</div>

				<div class="day">
					<div class="number">9</div>
				</div>
				
				<div class="day">
					<div class="number">10</div>
				</div>

				<div class="day">
					<div class="number">11</div>
				</div>

				<div class="day">
					<div class="number">12</div>
				</div>

				<div class="day disabled">
					<div class="number">13</div>
				</div>

				<div class="day">
					<div class="number">14</div>
				</div>

				<div class="day disabled">
					<div class="number">15</div>
				</div>

				<div class="day">
					<div class="number">16</div>
				</div>
				
				<div class="day">
					<div class="number">17</div>
				</div>

				<div class="day event">
					<div class="number">18</div>
				</div>

				<div class="day">
					<div class="number">19</div>
				</div>

				<div class="day event">
					<div class="number">20</div>
				</div>

				<div class="day">
					<div class="number">21</div>
				</div>

				<div class="day">
					<div class="number">22</div>
				</div>

				<div class="day">
					<div class="number">23</div>
				</div>
				
				<div class="day">
					<div class="number">24</div>
				</div>

				<div class="day">
					<div class="number">25</div>
				</div>

				<div class="day">
					<div class="number">26</div>
				</div>

				<div class="day">
					<div class="number">27</div>
				</div>

				<div class="day">
					<div class="number">28</div>
				</div>

				<div class="day">
					<div class="number">29</div>
				</div>

				<div class="day">
					<div class="number">30</div>
				</div>

				<div class="day">
					<div class="number">31</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>