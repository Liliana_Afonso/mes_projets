<?php 

// récupère le contenu du fichier
//$avengers = file_get_contents('texte.txt');
//echo $avengers; 


// remplace le contenu du fichier 
//file_put_contents ('texte.txt', 'new texte');

// Ajoute le contenu à la fin du fichier
//file_put_contents('texte.txt', 'New texte', FILE_APPEND);

// image
$im1 = imagecreatefromjpeg('image.jpg');
$im2 = imagecreatefromjpeg('image.jpg');
$im3 = imagecreatefromjpeg('image.jpg');
$im4 = imagecreatefromjpeg('image.jpg');


// ajout du filtre négatif
imagefilter ($im1, IMG_FILTER_NEGATE);
imagefilter ($im2,IMG_FILTER_GRAYSCALE );
imagefilter ($im3,IMG_FILTER_BRIGHTNESS , 200 );
imagefilter ($im4,IMG_FILTER_COLORIZE,-60,0,0,0 );



//Enregistrement de l'image dasn un nouveau fichier
imagejpeg($im1, 'image-negate.jpg', 90);
imagejpeg($im2, 'image-grayscale.jpg', 90);
imagejpeg($im3, 'image-brightness.jpg', 90);
imagejpeg($im4, 'image-colorize.jpg', 90);


// Libération de la mémoire
imagedestroy($im1);
imagedestroy($im2);
imagedestroy($im3);
imagedestroy($im4);

// Création d'une image vide et définition de la couleur 
//$im = imagecreatetruecolor(400, 300);
$text_color = imagecolorallocate($im, 145, 218, 15);

// Ajout du texte
imagestring ($im, 5, 20, 20, 'Wake up, CREA Genève!', $text_color);
imagestring($im, 5, 20, 40, '...', $text_color);
imagestring($im, 5, 20, 60, '...', $text_color);
imagestring($im, 5, 20, 90, 'Follow the White Rabbit', $text_color);

// Affichate dans le navigateur
header('Content-type: image/jpeg'); //(n'accete qu'une image de type jpeg NORMALEMENT ON SEPARE LA PARTIE TEXTE DANS UN AUTRE FICHIER PHP)
imagejpeg($im, NULL, 90);
imagedestroy($im);

?>
