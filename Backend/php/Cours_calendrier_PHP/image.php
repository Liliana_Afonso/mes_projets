<?php

/*
// Calcul des nouvelles dimensions
					$imsize = getimagesize('upload/Fleur5.png'); // Va prendre les dimenssion de l'image
					$NewImgW=$ORGW= $imsize[0];
					$NewImgH=$ORGH= $imsize[1];

                    // SI largueur > hauteur
					$width = 150;
					if (($ORGW>$ORGH) && ($ORGW> $width) )
					{
						$NewImgW = ($width * $ORGW) / $ORGH;
						$NewImgH = $width;
					}
					else if ($NewImgW > $width) 
					{
							$rognage = $NewImgW - $width; 
							$rognageCouper = $rognage/2;
						}
					echo $ORGW;
					
					$im = imagecreatefrompng('upload/Fleur5.png');

					//$newImage = imagecreatetruecolor(150, 150);
					//imagecopyresized($newImage,$im,0, 0, 0,0, 150,150 , $ORGW , $ORGH );

                    //imagefilter($im, IMG_FILTER_NEGATE);

                        header('Content-type:image/png');

					//imagepng($im, 'upload/' . $_FILES['image']['name'].'-modifier.png', 9); // le format png n'accepte que entre 0 et 9 alors que jpeg entre 0 et 100
					imagepng($im); // le format png n'accepte que entre 0 et 9 alors que jpeg entre 0 et 100
					
					imagedestroy($im);
*/

// Fichier et nouvelle taille
$filename = 'upload/Fleur5.png';

// Content type
header('Content-Type: image/png');

// Calcul des nouvelles dimensions
list($width, $height) = getimagesize($filename);
$newwidth = (150 * $width) / $height;
$newheight = 150;

$rognage = $newwidth - 150; 
$rognageCouper = $rognage/2;

// Chargement
$thumb = imagecreatetruecolor($newwidth, $newheight);
$source = imagecreatefrompng($filename);

// Redimensionnement
imagecopyresized($thumb, $source, 0, 0, $rognageCouper, 0, 150, 150, $width, $height);

// Affichage
imagepng($thumb);